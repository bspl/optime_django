import json

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from companies.models import CompanyAwards
from optimeusers.models import UserCompanyAward, OptimeUser


class AwardsView(APIView):
    """
    GET: returns all CompanyAwards
    POST: intakes an array including award ID and user ID and saves it as a new  usercompanyaward
    if its not already a company award else it creates a new usercompanyaward
    """

    def get(self, request):
        awards = CompanyAwards.objects.filter(company=request.user.company)

        return Response(data={'awards': list(awards.values())}, status=status.HTTP_200_OK)

    def post(self, request):
        # update winners with post
        update_awards = json.loads(request.POST.get('winners'))
        for award in update_awards:
            if award is not None:
                users = UserCompanyAward.objects.filter(user_id=int(award['id']),
                                                        company_award__award_id=int(award['award_id'])).values('user')
                if not users.exists():
                    award_update = UserCompanyAward()
                    award_update.company_award = CompanyAwards.objects.get(company=request.user.company,
                                                                           award_id=int(award['award_id']))
                    award_update.user = OptimeUser.objects.get(id=award['id'])
                else:
                    award_update = UserCompanyAward.objects.filter(company_award__award_id=int(award['award_id']),
                                                                   company_award__company=request.user.company).last()
                    award_update.company_award = CompanyAwards.objects.get(company=request.user.company,
                                                                           award_id=int(award['award_id']))
                    award_update.user = OptimeUser.objects.get(id=int(award['id']))
                award_update.save()
        return Response(status=status.HTTP_200_OK)
