from django.db import models
import uuid


def graphic_award_name(instance, filename):
    return 'graphic/{0}-{1}'.format(str(uuid.uuid4()), filename)


class Awards(models.Model):
    """"
        Awards contains information such as title, description, created at and graphic
    """
    title = models.CharField(max_length=255, null=False, blank=True, default='')
    description = models.TextField(default='', null=False, blank=True)
    created_at = models.DateTimeField(auto_created=True, null=False, blank=False)
    graphic = models.FileField(upload_to=graphic_award_name, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "awards"
