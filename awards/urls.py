from django.conf.urls import url
from .views import AwardsView

urlpatterns = [
    url(r'^$', AwardsView.as_view(), name='api-awards'),
]
