import tldextract

from companies.models import Company
from .settings import APP_PATHS


def _get_company_from_sub_domain(request):
    """Auxiliary function to obtain the company sub_domain (for company styling when the user is not authenticated)"""
    # first retrieve the host
    url = request.META['HTTP_HOST']

    # parse the url
    parse = tldextract.extract(url)
    sub_domain = parse.subdomain

    # ignore www
    if sub_domain == 'www':
        return None

    # fetch company based on this sub-domain
    if Company.objects.filter(subdomain__iexact=sub_domain).exists():
        return Company.objects.get(subdomain__iexact=sub_domain)
    else:
        return None


def active_page(request):
    """
        context processor exposes the current active page. Valid pages are defined in settings, and this function
        searches the url path for a match
        :param request: the web request, containing the url
        :return:    fields to add to the context
    """

    # parse the request URL
    url = request.get_full_path()
    url_split = url.split('/')

    # default to home
    return_value = 'home'

    for path in url_split:
        # skip keywords for sub apps
        if path in APP_PATHS['SUB_APPS']:
            continue

        if path in APP_PATHS['PATHS']:
            return_value = path

    return {
        'active_page': return_value,
    }


def company(request):
    """
        context processor to expose the company object in every view of the app
        :param request: the web request
        :return: an object containing the company object, or None if it was unable to retrieve this
    """
    if request.user is None or request.user.is_anonymous:
        the_company = _get_company_from_sub_domain(request)
    else:
        the_company = request.user.company

    return {
        'company': the_company
    }
