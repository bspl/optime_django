import os

from celery import Celery
from celery.schedules import crontab
from django.apps import apps
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'optime.settings')

ONE_YEAR_IN_SECONDS = 31557600

# The login credentials can also be set using the environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY,
# in that case the broker URL may only be sqs://
# app = Celery('optime', broker='sqs://') # sqs://aws_access_key_id:aws_secret_access_key@
app = Celery('optime')
app.conf.timezone = 'Europe/London'
app.conf.broker_transport_options = {'visibility_timeout': ONE_YEAR_IN_SECONDS}
# broker_transport_options = {'region': 'eu-west-1'}

app.conf.beat_schedule = {
    # Executes at midnight on teh first of every month,
    # as per https://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html#crontab-schedules.
    'monthly-deletion-of-goals': {
        'task': 'goals.tasks.deletion_of_goals',
        'schedule': crontab(0, 0, day_of_month='1'),
    },
}
app.config_from_object(settings)

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
