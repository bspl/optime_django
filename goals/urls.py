from django.conf.urls import url

from .views import GoalView, TaskView, WorkshopGoalView

urlpatterns = [
    url(r'^task/(?P<pk>[0-9]+)/$', TaskView.as_view(), name='api-tasks'),
    url(r'^$', GoalView.as_view(), name='api-goals'),
    url(r'^video/$', WorkshopGoalView.as_view(), name='api-workshop-goal'),
]
