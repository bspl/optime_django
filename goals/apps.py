from django.apps import AppConfig


class GoalsConfig(AppConfig):
    name = 'goals'

    def ready(self):
        try:
            import goals.tasks
        except ImportError:
            print('GoalsConfig - ImportError - unable to import goals.tasks')
