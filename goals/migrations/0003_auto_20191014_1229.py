# Generated by Django 2.2.6 on 2019-10-14 12:29

import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('goals', '0002_auto_20191010_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='start_date',
            field=models.DateField(default=datetime.date(2019, 10, 14)),
        ),
    ]
