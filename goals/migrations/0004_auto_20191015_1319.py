# Generated by Django 2.2.6 on 2019-10-15 13:19

import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('goals', '0003_auto_20191014_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='usergoal',
            name='is_active',
            field=models.BooleanField(default=False, help_text='is this my current goal?'),
        ),
        migrations.AlterField(
            model_name='goal',
            name='start_date',
            field=models.DateField(default=datetime.date(2019, 10, 15)),
        ),
    ]
