from datetime import datetime, timedelta

from oauth2_provider.admin import Application
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient

from content.models import VideoContent
from optimeusers.models import OptimeUser
from .models import Goal, VideoGoal, Task, UserGoal, UserTask
from .tasks import send_goal_reminder, SendStatus


class GoalsTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id,
                                                    client_secret=self.oauth_client_secret)

    def setUpLoggedInEmployee(self):
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test',
                               email_opt_in=True)
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpUserGoal(self):
        three_days_time = datetime.today() + timedelta(days=3)

        self.video = VideoContent(title='Test', created_at=datetime.today(),
                                  video='R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')
        self.video.save()

        self.goal = Goal(title='Test')
        self.goal.save()
        task_a = Task(title='Test', goal=self.goal)
        task_b = Task(title='Test', goal=self.goal)
        task_a.save()
        task_b.save()

        self.video_goal = VideoGoal(goal=self.goal, video=self.video)
        self.video_goal.save()

        self.user_goal = UserGoal(goal=self.video_goal, user=self.user, is_active=True)
        self.user_goal.save()

    def test_complete_task(self):
        self.setUpLoggedInEmployee()
        self.setUpUserGoal()

        self.assertEqual(self.user_goal.tasks.count(), self.goal.tasks.count())

        user_task = self.user_goal.tasks.all()[0]

        url = reverse('api-tasks', args=[user_task.pk])
        response = self.client.post(url, data={'is_complete': True})

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        task = UserTask.objects.get(pk=user_task.pk)

        self.assertEqual(task.is_complete, True)

    # tests for sending goal reminders
    # sending email- standard path
    def test_send_goal_reminder(self):
        self.setUpLoggedInEmployee()
        self.setUpUserGoal()

        result = send_goal_reminder(self.user.id, self.user_goal.id)

        self.assertEqual(result, SendStatus.SENT_EMAIL.value)

    # revoked consent
    def test_send_goal_reminder_no_consent(self):
        self.setUpLoggedInEmployee()
        self.setUpUserGoal()

        self.user.email_opt_in = False
        self.user.save()

        result = send_goal_reminder(self.user.id, self.user_goal.id)

        self.assertEqual(result, SendStatus.NO_PERMISSION.value)

    # has no active goal
    def test_send_goal_reminder_none_exists(self):
        self.setUpLoggedInEmployee()

        result = send_goal_reminder(self.user.id, 1)

        self.assertEqual(result, SendStatus.NO_GOAL_EXISTS.value)

    # has new active goal
    def test_send_goal_reminder_new_exists(self):
        self.setUpLoggedInEmployee()

        three_days_time = datetime.today() + timedelta(days=3)
        goal = Goal(title='Test')
        goal.save()

        video = VideoContent(title='Test', created_at=datetime.today(),
                             video='R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')
        video.save()

        video_goal = VideoGoal(goal=goal, video=video)
        video_goal.save()

        user_goal = UserGoal(goal=video_goal, user=self.user, is_active=True)
        user_goal.save()

        self.setUpUserGoal()

        result = send_goal_reminder(self.user.id, user_goal.id)

        self.assertEqual(result, SendStatus.NEW_ACTIVE_GOAL.value)

    # goal is complete
    def test_send_goal_reminder_goal_completed(self):
        self.setUpLoggedInEmployee()
        self.setUpUserGoal()

        for task in self.user_goal.tasks.all():
            task.is_complete = True
            task.save()

        result = send_goal_reminder(self.user.id, self.user_goal.id)

        self.assertEqual(result, SendStatus.GOAL_COMPLETE.value)
