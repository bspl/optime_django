from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from optimeusers.models import OptimeUser
from .models import VideoGoal, UserGoal, UserTask
from .serializers import UserTaskSerializer


# auxiliary function to return a 404 response
def _404_response():
    return Response(data={'error': {'goal': ['Could not find goal with this ID!']}}, status=status.HTTP_404_NOT_FOUND)


class GoalView(APIView):
    """
        post:
        selects a new VideoGoal for the user, creating the UserGoal object and making this the only active UserGoal
    """

    permission_classes = (IsAuthenticated,)

    # view selects a new active goal for the current user
    def post(self, request):
        goal_id = request.POST.get('goal')

        if goal_id is None:
            return _404_response()

        try:
            goal = VideoGoal.objects.get(pk=goal_id)

            # link this goal to user and make it the active goal
            user_goal = UserGoal(goal=goal, user=request.user, is_active=True)
            user_goal.save()

            return Response(status=status.HTTP_201_CREATED)

        except VideoGoal.DoesNotExist:
            return _404_response()


class WorkshopGoalView(APIView):
    """
        post:
        update current workshop goal
    """

    permission_classes = (IsAuthenticated,)

    # view selects a new active goal for the current user
    def post(self, request):
        video_id = request.POST.get('id')
        if video_id is None:
            return _404_response()

        try:
            goal = VideoGoal.objects.get(video_id=video_id)
            current_user = OptimeUser.objects.get(id=request.user.id)
            current_user.workshop_goal = goal
            current_user.save()

            try:
                user_goal = UserGoal.objects.get(user_id=request.user.id)
                user_goal.goal = goal
                user_goal.is_active = True
            except UserGoal.DoesNotExist:
                user_goal = UserGoal(user=request.user, goal=goal, is_active=True)
            user_goal.save()

            return Response(status=status.HTTP_201_CREATED)

        except VideoGoal.DoesNotExist:
            return _404_response()


class TaskView(APIView):
    """
        post:
        sets a parameterised Task to be completed
    """

    permission_classes = (IsAuthenticated,)

    def get_object(self, request, pk):
        try:
            task = UserTask.objects.get(pk=pk)
            self.check_object_permissions(request, task)
            return task
        except UserTask.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    # view to mark a task as completed (or not)
    def post(self, request, pk):
        task = self.get_object(request, pk)
        is_complete = request.POST.get('is_complete')

        if is_complete is None:
            return Response(data={'error': {'is_complete': ['is_complete must be True or False']}},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer = UserTaskSerializer(task, data={'is_complete': is_complete}, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)

        return Response(data={'error': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
