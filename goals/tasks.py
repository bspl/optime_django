from enum import Enum

from celery.utils.log import get_task_logger
from django.core.mail import send_mail
from django.template.loader import render_to_string

from optime.celery import app
from optime.settings import SECONDS_GOAL_REMINDER, FROM_EMAIL
from optimeusers.models import OptimeUser

logger = get_task_logger(__name__)


class SendStatus(Enum):
    NO_PERMISSION = 'no_permission'
    NO_GOAL_EXISTS = 'no_goal_exists'
    NEW_ACTIVE_GOAL = 'new_active_goal'
    GOAL_COMPLETE = 'goal_complete'
    SENT_EMAIL = 'sent_email'
    NO_USER_EXISTS = 'no_user_exists'


# celery task for submitting email notification to complete a goal
@app.task(name="send_goal_reminder")
def send_goal_reminder(user_id, video_goal_id):
    try:
        user = OptimeUser.objects.get(pk=user_id)

        # have I revoked permission?
        if not user.email_opt_in:
            logger.info('not sending email as I no longer consent')
            return SendStatus.NO_PERMISSION.value

        user_goal = user.active_goal

        # do I have an active goal?
        if user_goal is None:
            logger.error('abandoning email as goal did not exist!')
            return SendStatus.NO_GOAL_EXISTS.value

        video_goal = user_goal.goal

        # is this still my active goal?
        if video_goal_id != video_goal.pk:
            logger.info('not sending email as this is no longer my active goal')
            return SendStatus.NEW_ACTIVE_GOAL.value

        # have I completed the goal since this email was scheduled?
        if user_goal.is_complete:
            logger.info('not sending email as the goal has since been completed')
            return SendStatus.GOAL_COMPLETE.value

        email_context = {'goal': video_goal.goal.title}

        subject = 'Goal Reminder'
        html_content = render_to_string('email/goal_reminder.html', email_context)

        # send email notification if required
        if user.email is not None and len(user.email) > 4:
            send_mail(
                subject=subject,
                message='',
                from_mail=FROM_EMAIL,
                recipient_list=[user.email],
                fail_silently=False,
                html_message=html_content,

            )

            # reschedule another reminder for 1 week's time
            send_goal_reminder.apply_async((user_id, video_goal_id), countdown=SECONDS_GOAL_REMINDER)

            return SendStatus.SENT_EMAIL.value

    except OptimeUser.DoesNotExist:
        logger.error('abandoning email as user did not exist!')
        return SendStatus.NO_USER_EXISTS.value


@app.task()
def deletion_of_goals():
    from goals.models import UserGoal
    OptimeUser.objects.all().update(workshop_goal=None)
    UserGoal.objects.all().delete()
