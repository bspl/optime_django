from django.contrib import admin

from .models import Goal, Task, UserGoal


# inline display for recipe method steps
class TaskInline(admin.TabularInline):
    model = Task


class GoalAdmin(admin.ModelAdmin):
    fields = ('title', 'pillar')

    inlines = [TaskInline]


class UserGoalAdmin(admin.ModelAdmin):
    fields = ('goal', 'user', 'is_active')


admin.site.register(Goal, GoalAdmin)
admin.site.register(UserGoal, UserGoalAdmin)
