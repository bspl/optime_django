from django.db import models

from content.models import VideoContent, PillarTypeStatus
from optime.settings import SECONDS_GOAL_REMINDER
from optimeusers.models import OptimeUser
from .tasks import send_goal_reminder


class Goal(models.Model):
    """
        Goal model. Users can assign themselves a goal to achieve and receive reminders about

        Attributes:
            title   title of the goal, summarising what it entails
            pillar  the area of wellbeing which the goal focusses on
    """

    title = models.CharField(max_length=255, null=False, blank=False)
    pillar = models.CharField(max_length=16, choices=PillarTypeStatus.choices(), default=PillarTypeStatus.Mental)

    def __str__(self):
        return "(" + str(self.id) + "): " + self.title


class Task(models.Model):
    """
        A Task is a component of a goal. The goal is complete when all tasks have been completed

        Attributes:
            goal  the goal which the task is a part of
            title   title of the task, summarising what it entails
    """
    goal = models.ForeignKey(to=Goal, on_delete=models.CASCADE, blank=False, null=False, related_name='tasks')
    title = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return "(" + str(self.id) + "): " + self.title + " of goal " + str(self.goal.id)


class VideoGoal(models.Model):
    """
        Many to many relationship between Goal and VideoContent. The association is created when a video has a goal
        assigned to itx

        Attributes:
            goal  the goal which the task is a part of
            video   association to video
    """

    goal = models.ForeignKey(to=Goal, on_delete=models.CASCADE, blank=False, null=False)
    video = models.ForeignKey(to=VideoContent, on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
        return self.goal.title


class UserGoal(models.Model):
    """
        One to many relationship between OptimeUser and Goal. The association is created when the user selects a goal
        from a video they have watched

        Attributes:
            goal  the goal which the task is a part of
            user   association to user
            is_active   flag to indicate if this is the user's current active goal. Only one goal may be active at once
                        per user
            notification_lock   when the UserGoal is saved, a notification will be scheduled to remind the user. This
                                lock is set to true to prevent multiple notifications being scheduled (in fact the
                                notification task will schedule itself for another cycle)
            created_at  indicates creation time
            is_complete indicates if all of the associated tasks have been completed, and thus the goal is complete
    """

    goal = models.ForeignKey(to=VideoGoal, on_delete=models.CASCADE, blank=False, null=False, related_name='instances')
    user = models.ForeignKey(to=OptimeUser, on_delete=models.CASCADE, blank=False, null=False, related_name='goals')
    is_active = models.BooleanField(null=False, blank=False, default=False, help_text='is this my current goal?')
    # a lock to prevent notifications being triggered twice
    notification_lock = models.BooleanField(null=False, blank=False, default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        # detect creation
        is_creation = False
        if self.pk is None:
            is_creation = True

        # if this goal has become active, all other goals must become inactive
        if self.is_active is True:
            other_goals = self.user.goals.filter(is_active=True)

            for goal in other_goals:
                goal.is_active = False
                goal.save()

        # schedule a goal reminder for 1 week's time, if permission granted
        if self.user.email_opt_in and not self.notification_lock:
            send_goal_reminder.apply_async((self.user_id, self.goal_id), countdown=SECONDS_GOAL_REMINDER)
            self.notification_lock = True

        super(UserGoal, self).save(*args, **kwargs)

        # on create, create user tasks as well
        if is_creation is True:
            for task in self.goal.goal.tasks.all():
                user_task = UserTask(task=task, user_goal=self, is_complete=False)
                user_task.save()

    @property
    def is_complete(self):
        return not self.tasks.filter(is_complete=False).exists()


class UserTask(models.Model):
    """
        UserTasks are a component of a UserGoal, having a one to one relationship with this model and a one (UserTask)
        to many Tasks

        Attributes:
            task  association to task
            user_goal   association to UserGoal
            is_complete boolean flag set to True when the user has completed this task
    """

    task = models.ForeignKey(to=Task, on_delete=models.CASCADE, blank=False, null=False, related_name='instances')
    user_goal = models.ForeignKey(to=UserGoal, on_delete=models.CASCADE, blank=False, null=False, related_name='tasks')
    is_complete = models.BooleanField(null=False, blank=False, default=False, help_text='completion status')
