from rest_framework import serializers

from .models import UserTask


class UserTaskSerializer(serializers.Serializer):
    is_complete = serializers.BooleanField(allow_null=False)

    def create(self, validated_data):
        return UserTask.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.is_complete = validated_data.get('is_complete', instance.is_complete)

        instance.save()

        return instance
