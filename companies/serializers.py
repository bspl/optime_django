from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import *


class CompanySerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255, validators=[UniqueValidator(queryset=Company.objects.all())])
    logo = serializers.FileField(use_url=logo_media_name, allow_empty_file=True, max_length=None)

    def create(self, validated_data):
        return Company.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.logo = validated_data.get('logo', instance.logo)

        instance.save()

        return instance
