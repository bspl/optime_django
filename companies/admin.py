from django.contrib import admin
from .models import Company, CompanyAwards


class AwardsInline(admin.TabularInline):
    model = CompanyAwards


class CompanyAdmin(admin.ModelAdmin):
    fields = ('name', 'subdomain', 'logo', 'primary_colour', 'primary_accent_colour', 'secondary_colour',
              'introductory_questionnaire', 'requires_age', 'requires_gender', 'requires_weight', 'requires_height',
              'requires_ethnicity')
    inlines = [AwardsInline]

admin.site.register(Company, CompanyAdmin)
