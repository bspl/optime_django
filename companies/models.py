import uuid

from colorfield.fields import ColorField
from django.db import models
from optime.settings import DOMAIN_NAME
from awards.models import Awards


def logo_media_name(instance, filename):
    return 'logo/{0}-{1}'.format(str(uuid.uuid4()), filename)


class Company(models.Model):
    """
        The Company model. Represents a Company, one of the app's clients

        Attributes:
            subdomain   subdomain used by the company to access the site, e.g. company.optime.co.uk
            name    company name
            logo    image used as the company logo
            primary_colour  primary colour styling for this company
            primary_accent_colour   primary accent colour styling for this company
            secondary_colour    secondary colour styling for this company
            introductory_questionnaire  a questionnaire served to new employees after registration. Only one of these exists per company
    """

    subdomain = models.CharField(unique=True, max_length=32, null=True, blank=False,
                                 help_text='can be used by the company to access the site, e.g. company.optime.co.uk. '
                                           'Required to enable custom styling on login/registration pages')
    name = models.CharField(unique=True, max_length=255)
    logo = models.ImageField(upload_to=logo_media_name, null=True, blank=True, help_text='Optional company logo')
    primary_colour = ColorField(default='#25428A')
    primary_accent_colour = ColorField(default='#15327A')
    secondary_colour = ColorField(default='#00AEEF',
                                  help_text='NOTE: must not be set to white or it won\'t be visible on certain pages')
    introductory_questionnaire = models.ForeignKey(to='questionnaires.Questionnaire', on_delete=models.SET_NULL,
                                                   blank=True, null=True, related_name='company_introductory',
                                                   help_text='questionnaire served to new employees after registration')
    requires_age = models.BooleanField(null=False, blank=False, default=True)
    requires_gender = models.BooleanField(null=False, blank=False, default=True)
    requires_weight = models.BooleanField(null=False, blank=False, default=True)
    requires_height = models.BooleanField(null=False, blank=False, default=True)
    requires_ethnicity = models.BooleanField(null=False, blank=False, default=True)
    requires_activity = models.BooleanField(null=False, blank=False, default=True)

    def awards(self):
        qqs = CompanyAwards.objects.filter(company=self).order_by('order')

        # return the questions in order
        questions = []
        for q in qqs:
            questions.append(q.question)

        return questions

    def save(self, *args, **kwargs):
        # attempting to save reserved domain as subdomain?
        if self.subdomain is not None:
            if self.subdomain == DOMAIN_NAME:
                raise ValueError('Cannot have ' + DOMAIN_NAME + ' as your subdomain!')

            # force lowercase
            self.subdomain = self.subdomain.lower()

        super(Company, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "companies"


class CompanyAwards(models.Model):
    """
    CompanyAwards holds a ManyToMany relationship betweeen companies and awards
    """
    company = models.ForeignKey(to=Company, on_delete=models.CASCADE, blank=True, null=True)
    award = models.ForeignKey(to=Awards, on_delete=models.CASCADE, blank=False, null=True)

    def __str__(self):
        return "company " + str(self.company) + " award " + str(self.award)

    class Meta:
        verbose_name_plural = "company awards"
