# Generated by Django 2.2.6 on 2019-12-11 14:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0012_companyawards'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CompanyAwards',
        ),
    ]
