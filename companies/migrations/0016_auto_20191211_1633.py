# Generated by Django 2.2.6 on 2019-12-11 16:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0015_companyawards_company'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companyawards',
            name='award',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='awards.Awards'),
        ),
    ]
