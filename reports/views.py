from goals.models import UserGoal, PillarTypeStatus
from optimeusers.models import OptimeUser, UserTypeStatus
from questionnaires.models import Questionnaire, WellbeingQuestionnaireInstance, QuestionnaireResponse, \
    ScaleQuestionnaireQuestionResponse, SelfAssessmentQuestionResponse


def _get_mean_average(responses):
    """
        :param responses: an iterable of responses (integers)
        :return: the mean average of the responses
    """
    total = 0

    for response in responses:
        total += response

    # avoid division by zero
    if len(responses) == 0:
        return 0

    return total / len(responses)


def get_radar_chart_data(user):
    """
        Auxiliary function returns the data necessary for the frontendsite radar chart

        :param user: the user to build the chart around
        :return: a list of dictionaries, one for each pillar (wellbeing category), containing the name of the pillar
        (in code and user-friendly formats) and the mean average input for this pillar
    """
    radar_data = []

    # get valid wellbeing questionnaires for my company
    questionnaires = Questionnaire.objects.filter(company=user.company). \
        values_list('pk', flat=True)

    # get my questionnaire responses to these that are under self Reflection
    responses = QuestionnaireResponse.objects.filter(user=user, questionnaire_id__in=questionnaires)
    filter_responses_by_question = []
    for response in responses:
        for question in response.questionnaire.questions:
            if question.self_assessment and question.response_type == 'scale':
                filter_responses_by_question.append(response.id)

    # get the scale question responses from these surveys
    scales = ScaleQuestionnaireQuestionResponse.objects.filter(questionnaire_response_id__in=filter_responses_by_question)

    # get all self Reflection question responses
    self_assessment_questions = SelfAssessmentQuestionResponse.objects.filter(user=user)

    # iterate over the pillars
    for pillar in PillarTypeStatus.choices():
        # data is the mean of the responses of this pillar type
        pillar_responses = list(r.response for r in scales if r.questionnaire_question.question.pillar == pillar[0])

        self_assessment_pillar_responses = list(r.response for r in self_assessment_questions if r.question.pillar == pillar[0])

        all_responses = pillar_responses + self_assessment_pillar_responses

        mean_avg = _get_mean_average(all_responses)

        radar_data.append({
            'name_user': pillar[1],
            'name_code': pillar[0],
            'data': mean_avg
        })

    return radar_data


def get_survey_engagement_data(user, start_date=None, end_date=None):
    """
        Returns survey completion data for a company's engagement report (for the employer dashboard)

        :param user: the user who made the request
        :param start_date: the starting date for the report (filter)
        :param end_date: the ending data for the report (filter)
        :return: a list containing a single dictionary for the survey completion of all employees. Designed this way
        to be easily extendable for a breakdown on employee-by-employee or group-by-group
    """
    # permissions check
    if user.user_type != UserTypeStatus.Employer:
        raise PermissionError('user must be an employer to access this data!')

    result = []

    # get valid wellbeing questionnaires for my company
    questionnaires = Questionnaire.objects.filter(company=user.company)

    if start_date is not None:
        questionnaires = questionnaires.exclude(created_at__lte=start_date)
    if end_date is not None:
        questionnaires = questionnaires.exclude(created_at__gte=end_date)

    # get all employee questionnaire responses to these surveys
    responses = QuestionnaireResponse.objects.filter(questionnaire__in=questionnaires)

    if start_date is not None:
        responses = responses.exclude(created_at__lte=start_date)
    if end_date is not None:
        responses = responses.exclude(created_at__gte=end_date)

    questionnaires_count = questionnaires.count()

    # add one column for all employees
    if questionnaires_count == 0:
        # returning None to indicate there is no data
        return None
    else:
        # questionnaires * employees (excluding employers and inactive users)
        total_instances = questionnaires_count * OptimeUser.objects.filter(company=user.company,
                                                                           user_type=UserTypeStatus.Employee,
                                                                           is_active=True,
                                                                           email_confirmed=True).count()

        if total_instances == 0:
            return None

        result.append({
            'employee': 'All Employees',
            'percentage': (responses.count() / total_instances) * 100
        })

    return result


def get_goal_completion_data(user, start_date=None, end_date=None):
    """
        Returns goal completion data for a company's engagement report (for the employer dashboard)

        :param user: the user who made the request
        :param start_date: the starting date for the report (filter)
        :param end_date: the ending data for the report (filter)
        :return: a list containing a single dictionary for the goal completion of all employees. Designed this way
        to be easily extendable for a breakdown on employee-by-employee or group-by-group
    """
    # permissions check
    if user.user_type != UserTypeStatus.Employer:
        raise PermissionError('user must be an employer to access this data!')

    result = []

    # get all user goals for this company
    user_goals = UserGoal.objects.filter(user__company=user.company, user__is_active=True)

    if start_date is not None:
        user_goals = user_goals.exclude(created_at__lte=start_date)
    if end_date is not None:
        user_goals = user_goals.exclude(created_at__gte=end_date)

    user_goals_count = user_goals.count()

    # add one column for all employees
    if user_goals_count == 0:
        # returning None to indicate there is no data
        return None
    else:
        complete_goals = list(g.pk for g in user_goals if g.is_complete)

        result.append({
            'employee': 'All Employees',
            'percentage': (len(complete_goals) / user_goals_count) * 100
        })

    return result


def get_employee_engagement_data(user, start_date=None, end_date=None):
    """
        Returns the engagement report to display to the user in the employer dashboard

        :param user: the user who made the request
        :param start_date: the starting date for the report (filter)
        :param end_date: the ending data for the report (filter)
        :return: a dictionary containing the survey and goal completion data
    """
    # permissions check
    if user.user_type != UserTypeStatus.Employer:
        raise PermissionError('user must be an employer to access this data!')

    engagement_data = {
        'surveys': get_survey_engagement_data(user, start_date, end_date),
        'goals': get_goal_completion_data(user, start_date, end_date)
    }

    return engagement_data


def get_employee_wellbeing_data(user, start_date=None, end_date=None):
    """
        Returns the employee wellbeing data over time for the employer dashboard

        :param user: the user who made the request
        :param start_date: the starting date for the report (filter)
        :param end_date: the ending data for the report (filter)
        :return: a dictionary containing wellbeing data over time. Returns None if there is no such data. The data is
        structured where dates are defined by the different wellbeing questionnaires sent over time, and the values
        are defined by the mean average of responses to wellbeing questions of scale type. A different data set is
        included for each pillar (wellbeing category)
    """
    # permissions check
    if user.user_type != UserTypeStatus.Employer:
        raise PermissionError('user must be an employer to access this data!')

    wellbeing_data = {}

    # get valid wellbeing questionnaires for my company
    wellbeing_questionnaires = WellbeingQuestionnaireInstance.objects.filter(company=user.company)

    # filter these by date if that's passed
    if start_date is not None:
        wellbeing_questionnaires = wellbeing_questionnaires.exclude(created_at__lte=start_date)
    if end_date is not None:
        wellbeing_questionnaires = wellbeing_questionnaires.exclude(created_at__gte=end_date)

    if wellbeing_questionnaires.count() == 0:
        return None

    wellbeing_data['dates'] = []
    wellbeing_data['pillars'] = []

    # set up the pillars data
    for pillar in PillarTypeStatus.choices():
        wellbeing_data['pillars'].append({
            'name_user': pillar[1],
            'name_code': pillar[0],
            'data': []
        })

    # iterate over questionnaires
    for questionnaire in wellbeing_questionnaires:

        # TODO: smarter selection of date labels
        wellbeing_data['dates'].append(questionnaire.created_at)

        # get all employee questionnaire responses to this survey
        responses = QuestionnaireResponse.objects.filter(questionnaire=questionnaire, user__is_active=True) \
            .values_list('pk', flat=True)

        # get the scale question responses for this survey
        scales = ScaleQuestionnaireQuestionResponse.objects.filter(questionnaire_response_id__in=responses)

        # iterate over the pillars
        for i, pillar in enumerate(PillarTypeStatus.choices()):
            # data is the mean of the responses of this pillar type
            pillar_responses = list(r.response for r in scales if r.questionnaire_question.question.pillar == pillar[0])

            mean_avg = _get_mean_average(pillar_responses)

            wellbeing_data['pillars'][i]['data'].append(mean_avg)

    return wellbeing_data
