import calendar
from datetime import datetime
from itertools import chain

from django.db.models import Q
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from goals.models import UserGoal
from questionnaires.models import Questionnaire, QuestionnaireResponse, SelfAssessmentAlert
from .models import RecipeContent, ArticleContent, NewsletterContent, VideoContent
from .serializers import VideoRecommendationsSerializer

START_DATE = datetime(2019, 1, 1, 0, 0)


def _get_publish_date(elem):
    """
        auxiliary function compares content objects by sorting key

        :param elem: the element to compare
        :return: the publish_date property of elem
    """
    return elem.publish_date


def _get_month_date_min_max(date):
    """
        auxiliary function gets the minimum and maximum dates of the month of a specified date

        :param date: a specific day of the year
        :return: a dictionary containing the minimum date in this month, and the maximum date
    """
    bounds = dict()

    bounds['min_date'] = datetime(date.year, date.month, 1, 0, 0, 0, 0)

    max_day_of_month = calendar.monthrange(date.year, date.month)[1]
    bounds['max_date'] = min(datetime.now(), datetime(date.year, date.month, max_day_of_month, 23, 59, 59, 59))

    return bounds


def _get_video_goal_pks():
    """
        :return: a generator containing video_goal primary keys
    """
    return (v.pk for v in VideoContent.objects.all() if v.has_goals)


def get_goals_video(date=datetime.now()):
    """
        View to GET specified month's goals video (a video with goals attached)
        :param date: a date within the month to fetch
        :return: the first goals video published in this month, or None if none exists
    """
    goals_videos = _get_video_goal_pks()
    result = list(VideoContent.objects.filter(pk__in=goals_videos))

    return result

def get_recipe_by_diet_reqs(user, start_date, end_date):

    true_diet_reqs = build_recipe_query(user)
    true_diet_reqs.add(Q(publish_date__gte=start_date), Q.AND)
    true_diet_reqs.add(Q(publish_date__lte=end_date), Q.AND)

    return RecipeContent.objects.filter(true_diet_reqs)



def get_published_content(user, start_date=START_DATE, end_date=datetime.now()):
    """
        Helper function to GET all published content between specified dates, defaulting to all content ever.
        Sorted newest first.
        Goals videos are excluded
    """
    # never trust user input
    if end_date > datetime.now() or end_date is None:
        end_date = datetime.now()

    if start_date > datetime.now() or start_date is None:
        start_date = START_DATE

    recipe_contents = get_recipe_by_diet_reqs(user, start_date, end_date)
    # build content list from each content type
    content = list(chain(
        VideoContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date),
        recipe_contents,
        ArticleContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date,
                                      occupational_activity=user.occupational_activity),
        NewsletterContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date), )
    )

    # sort content by date created
    content.sort(key=_get_publish_date, reverse=True)

    return content


def get_published_content_by_pillar(user, pillar):
    """
        Get all published content by pillar
    """
    # build content list from each content type
    end_date = datetime.now()
    start_date = START_DATE

    recipe_contents = get_recipe_by_diet_reqs(user, start_date, end_date)

    # build content list from each content type
    content = list(chain(
        VideoContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date, pillar=pillar),
        recipe_contents,
        ArticleContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date, pillar=pillar,
                                      occupational_activity=user.occupational_activity),
        NewsletterContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date, pillar=pillar), )
    )

    # sort content by date created
    content.sort(key=_get_publish_date, reverse=True)

    return content


def get_published_content_by_content_type(user, content_type):
    """
        Get all published content by type
    """
    end_date = datetime.now()
    start_date = START_DATE

    if content_type == 'article':
        return ArticleContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date,
                                             occupational_activity=user.occupational_activity).order_by(
            '-publish_date')
    elif content_type == 'video':
        return VideoContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date).order_by(
            '-publish_date')
    elif content_type == 'newsletter':
        return NewsletterContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date).order_by(
            '-publish_date')
    elif content_type == 'recipe':
        return get_recipe_by_diet_reqs(user, start_date, end_date).order_by('-publish_date')
    else:
        # build content list from each content type
        content = list(chain(
            VideoContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date),
            RecipeContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date,
                                         gluten_free=user.gluten_free, meat_free=user.meat_free, vegan=user.vegan,
                                         dairy_free=user.dairy_free),
            ArticleContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date,
                                          occupational_activity=user.occupational_activity),
            NewsletterContent.objects.filter(publish_date__gte=start_date, publish_date__lte=end_date), )
        )

        # sort content by date created
        content.sort(key=_get_publish_date, reverse=True)

        return content


def get_published_content_of_month(user, date=datetime.now()):
    """
        auxiliary function to GET all published content in the same month as date passed
    """
    # never trust user input
    if date > datetime.now() or date is None:
        date = datetime.now()

    # start_date is the 1st of this month, end_date the last day
    date_bounds = _get_month_date_min_max(date)

    return get_published_content(user, date_bounds['min_date'], date_bounds['max_date'])


def get_new_alerts(user, limit=None):
    """
        View to get alerts. Alerts returned are actually Content objects which have been published since the user
        was last online. This is done to save database resources. Goals videos are excluded

        :param user: the logged-in user
        :param limit: an optional limit on the number of alerts returned
        :return: a list of content objects sorted most recently published first
    """
    now = datetime.now()

    # if the user joined very recently, default to the date they joined
    last_last_login = user.last_last_login
    if last_last_login is None:
        last_last_login = user.date_joined

    # exclude goals videos
    goals_videos = _get_video_goal_pks()

    # if the user has not yet answered a questionniare (i.e. they're a new user),
    # then their questionnaire is the most recent one regardless of publish date, otherwise,
    #  base on publish date being > last last login
    if QuestionnaireResponse.objects.exists():
        # get current questionnaire alert id and id of user completed questionnaires
        current_questionnaire_alert = Questionnaire.objects.filter(is_published=True,
                                                                   publish_date__gte=last_last_login,
                                                                   company=user.company).values_list('id',
                                                                                                     flat=True).first()
        user_completed_questionnaires = QuestionnaireResponse.objects.filter(user=user).values_list('questionnaire_id',
                                                                                                    flat=True)

        published_questionniares = Questionnaire.objects.filter(is_published=True,
                                                                publish_date__gte=last_last_login,
                                                                company=user.company).exclude(
            id=current_questionnaire_alert)
    else:
        # get current questionnaire alert id and id of user completed questionnaires
        current_questionnaire_alert = Questionnaire.objects.filter(is_published=True, company=user.company).values_list(
            'id',
            flat=True).first()
        user_completed_questionnaires = QuestionnaireResponse.objects.filter(user=user).values_list('questionnaire_id',
                                                                                                    flat=True)

        published_questionniares = Questionnaire.objects.filter(is_published=True, company=user.company).exclude(
            id=current_questionnaire_alert)[:1]

    # if QuestionnaireResponse.objects.exists():
    #     # get current questionnaire alert id and id of user completed questionnaires
    #     current_awards_alert = Awards.objects.filter(is_published=True,
    #                                                                publish_date__gte=last_last_login,
    #                                                                company=user.company).values_list('id',
    #                                                                                                  flat=True).first()

    #     published_awards = Awards.objects.filter(is_published=True,
    #                                                             publish_date__gte=last_last_login,
    #                                                             company=user.company).exclude(
    #         id=current_awards_alert)
   

    # build content list from each content type
    content = list(chain(
        VideoContent.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login).exclude(
            pk__in=goals_videos),
        RecipeContent.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login,
                                     gluten_free=user.gluten_free, meat_free=user.meat_free, vegan=user.vegan,
                                     dairy_free=user.dairy_free),
        ArticleContent.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login,
                                      occupational_activity=user.occupational_activity).exclude(
            occupational_activity__isnull=False),
        NewsletterContent.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login, company=None),
        NewsletterContent.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login,
                                         company=user.company),
        SelfAssessmentAlert.objects.filter(publish_date__lte=now, publish_date__gte=last_last_login),
        published_questionniares
        if user_completed_questionnaires.filter(questionnaire_id=current_questionnaire_alert,
                                                questionnaire__company=user.company).exists()
        else Questionnaire.objects.filter(is_published=True, publish_date__gte=last_last_login, company=user.company),

    ))

    # sort content by date created
    content.sort(key=_get_publish_date, reverse=True)

    if limit is not None:
        content = content[:limit]

    return content


def get_video_recommendations(user, limit=None):
    # get users current goal and pillar
    user_goal = UserGoal.objects.filter(user=user, is_active=True).first()
    pillar_name = user_goal.goal.goal.pillar

    # we want recommendations based of the popularity of the video content in relation to the pillar
    def get_content(pillar):
        # count all video goals that have the same pillar as the user goal
        video_content = []
        for content in VideoContent.objects.all():
            # if the video content pillar matches the users selected goal pillar then count
            if not content.has_goals and content.pillar == pillar:
                video_content.append(content)
        # check that there is video content
        if any(video_content):
            return video_content
        else:
            content = None
        return content

    return get_content(pillar_name)


class VideoRecommendationsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        # get current user
        user = request.user

        # optional param to limit the number returned
        limit = request.query_params.get('limit')

        content = get_video_recommendations(user, limit)

        serializer = VideoRecommendationsSerializer(content, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

def build_recipe_query(user):
    users_dietary_reqs = {'vegan': user.vegan,
                          'gluten_free': user.gluten_free,
                          'meat_free': user.meat_free,
                          'dairy_free': user.dairy_free
                          }

    true_diet_reqs = Q()
    for key, value in users_dietary_reqs.items():
        if value:
            true_diet_reqs.add(Q(**{key: value}), Q.AND)
    return true_diet_reqs

def recipe_meets_dietary_reqs(recipe, user):
    """
        returns True if a given recipe matches user dietary requirements
    """

    filter_recipe_query = build_recipe_query(user)
    recipes = RecipeContent.objects.filter(filter_recipe_query)

    return True if recipe in recipes else False
