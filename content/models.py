import uuid
from enum import Enum

from django.db import models

from companies.models import Company
from optimeusers.models import OptimeUser, OccupationalActivityStatus


def recipe_media_name(instance, filename):
    return 'image/recipe/{0}-{1}'.format(str(uuid.uuid4()), filename)


def article_media_name(instance, filename):
    return 'image/article/{0}-{1}'.format(str(uuid.uuid4()), filename)


def newsletter_media_name(instance, filename):
    return 'newsletter/{0}-{1}'.format(str(uuid.uuid4()), filename)


def video_media_name(instance, filename):
    return 'video/{0}-{1}'.format(str(uuid.uuid4()), filename)


class PillarTypeStatus(object):
    """
        defines the pillars (categories of wellbeing) in the application
    """

    Mental = 'mental'
    Physical = 'physical'
    Professional = 'professional'
    Environmental = 'environmental'
    Social = 'social'
    Financial = 'financial'

    @classmethod
    def choices(cls):
        return (
            (cls.Mental, 'Mental and Emotional'),
            (cls.Physical, 'Physical'),
            (cls.Professional, 'Professional'),
            (cls.Environmental, 'Environmental'),
            (cls.Social, 'Social'),
            (cls.Financial, 'Financial'),
        )


class ContentType(Enum):
    RECIPE = 'recipe'
    ARTICLE = 'article'
    VIDEO = 'video'
    NEWSLETTER = 'newsletter'
    # NOTE: QUESTIONNAIRE is included here to facilitate its inclusion in the alerts system. It does not extend the
    # Content class
    QUESTIONNAIRE = 'questionnaire'

    # NOTE: SELF_ASSESSMENT is included here to facilitate its inclusion in the alerts system. It does not extend the
    # Content class
    SELF_ASSESSMENT = 'self_assessment'


# NOTE: this is not an abstract base class because Content objects are used as alerts
class Content(models.Model):
    """
        The Content model. Base class containing fields common to all types of content. Content is served to the user
        via index and archived pages, and is served to all users across all companies

        Attributes:
            title   the title of the article, recipe, or other content type
            created_at  the date/time the content was created
            publish_date    the date/time the content is published to users
            image   image to be served alongside the content
            pillar  the wellbeing category that this content is associated with
    """

    title = models.CharField(max_length=255, null=False, blank=False)
    created_at = models.DateTimeField(auto_created=True, null=False, blank=False)
    publish_date = models.DateTimeField(null=False, blank=False,
                                        help_text='the time the content should go live. Set to the distant future to make unpublished')
    image = models.ImageField(upload_to=article_media_name, null=True, blank=False, help_text='Required image')
    pillar = models.CharField(max_length=16, choices=PillarTypeStatus.choices(), default=PillarTypeStatus.Mental)

    @property
    def content_type(self):
        raise NotImplementedError('This is an abstract class!')

    def __str__(self):
        return str(self.created_at)


class RecipeContent(Content):
    """
        Model representing a recipe

        Attributes:
            ingredients   a text list of required ingredients
            dairy_free  indicates the recipe lacks dairy
            meat_free  indicates the recipe is suitable for vegetarians
            gluten_free  indicates the recipe lacks gluten
            vegan   indicates the recipe is suitable for vegans
    """

    ingredients = models.TextField(default='', null=False, blank=True, help_text='list of ingredients')
    dairy_free = models.BooleanField(null=False, blank=False, default=False)
    meat_free = models.BooleanField(null=False, blank=False, default=False)
    gluten_free = models.BooleanField(null=False, blank=False, default=False)
    vegan = models.BooleanField(null=False, blank=False, default=False)

    @property
    def content_type(self):
        return ContentType.RECIPE

    def steps(self):
        """
            :return:    recipe steps in their configured order
        """
        return self.method_steps.all().order_by('order')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Recipe'
        verbose_name_plural = 'Recipes'


class RecipeStep(models.Model):
    """
        A recipe step is an individual stage required to complete a recipe

        Attributes:
            recipe  the recipe the step belongs to
            order   the step's place in the process
            description a description of what the step entails
    """

    recipe = models.ForeignKey(to=RecipeContent, on_delete=models.CASCADE, blank=False, null=False,
                               related_name='method_steps')
    order = models.PositiveIntegerField(null=False, blank=False, help_text='number step in the process')
    description = models.TextField(default='', null=False, blank=True, help_text='description of method')

    class Meta:
        verbose_name = 'Recipe step'
        verbose_name_plural = 'Recipe steps'


class ArticleContent(Content):
    """
        Model representing an article

        Attributes:
            content the article in HTML format
    """

    content = models.TextField(default='', null=False, blank=True, help_text='Article content')
    occupational_activity = models.CharField(max_length=16, choices=OccupationalActivityStatus.choices(),
                                             null=True,
                                             blank=True,
                                             help_text='amount of strenuous activity involved in this article.')

    @property
    def content_type(self):
        return ContentType.ARTICLE

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'


class NewsletterContent(Content):
    """
        Model representing a newsletter

        Attributes:
            newsletter  the newsletter, typically in PDF format
    """

    newsletter = models.FileField(null=False, blank=False, upload_to=newsletter_media_name)

    company = models.ForeignKey(to=Company, on_delete=models.CASCADE, blank=True, null=True)

    @property
    def content_type(self):
        return ContentType.NEWSLETTER

    class Meta:
        verbose_name = 'Newsletter'
        verbose_name_plural = 'Newsletters'


class VideoContent(Content):
    """
        Model representing a video content. Videos have a foreign key relation with Goals (one video can have many
        goals). If a video has no goals attached to it (see the has_goals property), then it behaves like the other
        content models in this file. If it has one or more goal, however, then it is known as a 'goals video'. Goals
        videos are served to the user periodically, and are included so that the user can select a goal. These videos
        are not served in the archive

        Attributes:
            video   the video to show
            description a description of the video
    """

    video = models.FileField(null=False, blank=False, upload_to=video_media_name)
    description = models.TextField(default='', null=False, blank=True, help_text='video description')

    @property
    def content_type(self):
        return ContentType.VIDEO

    @property
    def has_goals(self):
        """
            property to identify if this is a goals video or not
            :return: True if the video is associated with one or more goal
        """
        from goals.models import VideoGoal
        return VideoGoal.objects.filter(video=self).exists()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'
