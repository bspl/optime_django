# Generated by Django 2.2.5 on 2019-10-02 14:47

import django.db.models.deletion
from django.db import migrations, models

import content.models


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0002_articlecontent'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsletterContent',
            fields=[
                ('content_ptr',
                 models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                      primary_key=True, serialize=False, to='content.Content')),
                ('newsletter', models.FileField(upload_to=content.models.newsletter_media_name)),
            ],
            bases=('content.content',),
        ),
    ]
