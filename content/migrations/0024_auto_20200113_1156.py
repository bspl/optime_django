# Generated by Django 2.2.6 on 2020-01-13 11:56

import content.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0023_auto_20200108_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='image',
            field=models.ImageField(help_text='optional image', null=True, upload_to=content.models.article_media_name),
        ),
    ]
