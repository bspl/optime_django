# Generated by Django 2.2.6 on 2019-10-25 13:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0012_videocontent_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipecontent',
            name='contains_nuts',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='recipecontent',
            name='dairy_free',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='recipecontent',
            name='gluten_free',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='recipecontent',
            name='meat_free',
            field=models.BooleanField(default=False),
        ),
    ]
