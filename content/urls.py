from django.conf.urls import url

from .views import VideoRecommendationsView

urlpatterns = [
    url(r'^video/recommendations/$', VideoRecommendationsView.as_view(), name='api-video-recommendations'),
]
