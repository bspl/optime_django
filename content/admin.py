from django.contrib import admin

from goals.models import VideoGoal
from .models import Content, RecipeContent, RecipeStep, ArticleContent, NewsletterContent, VideoContent


class ContentAdmin(admin.ModelAdmin):
    model = Content
    fields = ('title', 'created_at', 'publish_date', 'image', 'pillar')


# inline display for recipe method steps
class RecipeStepInline(admin.TabularInline):
    model = RecipeStep


class RecipeContentAdmin(ContentAdmin):
    fields = ('ingredients', 'dairy_free', 'meat_free', 'gluten_free', 'vegan') + ContentAdmin.fields
    inlines = [RecipeStepInline]


class ArticleContentAdmin(ContentAdmin):
    fields = ('content', 'occupational_activity',) + ContentAdmin.fields


class VideoGoalInline(admin.TabularInline):
    model = VideoGoal


class VideoContentAdmin(ContentAdmin):
    fields = ('video', 'description',) + ContentAdmin.fields
    inlines = [VideoGoalInline]


admin.site.register(RecipeContent, RecipeContentAdmin)
admin.site.register(ArticleContent, ArticleContentAdmin)
admin.site.register(NewsletterContent)
admin.site.register(VideoContent, VideoContentAdmin)
