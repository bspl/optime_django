from rest_framework import serializers

from .models import Content, PillarTypeStatus


class ContentSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    title = serializers.CharField(required=True, allow_blank=False, max_length=255)
    created_at = serializers.DateTimeField(read_only=True)
    publish_date = serializers.DateTimeField()
    content_type = serializers.SerializerMethodField()
    pillar = serializers.ChoiceField(required=True, choices=PillarTypeStatus.choices(), allow_blank=False)

    def get_content_type(self, content):
        return content.content_type.value

    def create(self, validated_data):
        return Content.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.publish_date = validated_data.get('publish_date', instance.publish_date)
        instance.pillar = validated_data.get('pillar', instance.pillar)

        instance.save()

        return instance


class VideoRecommendationsSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    title = serializers.CharField(required=True, allow_blank=False, max_length=255)
    created_at = serializers.DateTimeField(read_only=True)
    pillar = serializers.ChoiceField(required=True, choices=PillarTypeStatus.choices(), allow_blank=False)
    image = serializers.ImageField()
    video = serializers.FileField(required=True)

    def get_video(self, content):
        return content.video
