from .views import get_new_alerts


def alerts(request):
    """
        context processor exposes content to alert the user about in the page
        :param request: the web request
        :return:    fields to add to the context
    """

    if request.user.is_anonymous:
        return {
            'alerts': None
        }

    content = get_new_alerts(request.user, 6)
    questionare = [alert for alert in content if alert.content_type.value == "questionnaire"]

    return {
        'alerts': content,
        'questionnaire_alert': questionare
    }
