from datetime import datetime, timedelta

from dateutil import parser
from oauth2_provider.admin import Application
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient

from content.views import get_published_content_by_pillar, get_published_content_by_content_type
from optimeusers.models import OptimeUser
from .models import Content, VideoContent, ArticleContent, RecipeContent
from goals.models import UserGoal, Goal, VideoGoal
from collections import OrderedDict
from dateutil import parser


class AlertsTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id,
                                                    client_secret=self.oauth_client_secret)

    def setUpLoggedInEmployee(self):
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test')
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpContent(self):
        self.content = []
        recipe = Content(title='Title', created_at=datetime.now())
        article = Content(title='Title', created_at=datetime.now())
        video = Content(title='Title', created_at=datetime.now())

        recipe.save()
        article.save()
        video.save()

        self.content.append(recipe)
        self.content.append(article)
        self.content.append(video)

    def test_get_all_alerts(self):
        self.setUpLoggedInEmployee()
        self.setUpContent()

        url = reverse('api-alerts')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), len(self.content))

    def test_get_top_alerts(self):
        self.setUpLoggedInEmployee()
        self.setUpContent()

        url = reverse('api-alerts')
        response = self.client.get(url, {'limit': 2})

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertGreater(len(self.content), 2)
        self.assertEqual(len(response.data), 2)


class VideoRecommendationTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id,
                                                    client_secret=self.oauth_client_secret)

    def setUpEmployees(self):
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test')
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpUserGoal(self):
        four_days_time = datetime.today() + timedelta(days=4)
        self.goal = Goal(title='Test', end_date=four_days_time)
        self.goal.save()

        self.video_content = VideoContent(title='Title', created_at=parser.parse('12/10/2019'), video='test')
        self.video_content.save()
        self.video_goal = VideoGoal(goal=self.goal, video=self.video_content)
        self.video_goal.save()

        self.user_goal = UserGoal(goal=self.video_goal, user=self.user, is_active=True)
        self.user_goal.save()

    def setUpVideoContent(self):
        self.video_content1 = VideoContent(title='Title1', created_at=parser.parse('21/10/2019'), video='test1')
        self.video_content1.save()

        self.video_content2 = VideoContent(title='Title2', created_at=parser.parse('21/10/2019'), video='test1')
        self.video_content2.save()

    def test_get_video_recommendations(self):
        self.setUpEmployees()
        self.setUpUserGoal()
        self.setUpVideoContent()

        url = reverse('api-video-recommendations')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.response_items_first = list(response.data[0].items())
        self.response_items_second = list(response.data[1].items())

        self.assertEqual(len(response.data), 2)

        # check all fields are matching for first response
        self.assertEqual(self.response_items_first[0][1], 2)
        self.assertEqual(self.response_items_first[1][1], 'Title1')
        self.assertEqual(self.response_items_first[2][1], '2019-10-21T00:00:00Z')
        self.assertEqual(self.response_items_first[3][1], 'mental')
        self.assertEqual(self.response_items_first[4][1], None)
        self.assertEqual(self.response_items_first[5][1], '/media/test1')

        # check all fields are matching for second response
        self.assertEqual(self.response_items_second[0][1], 3)
        self.assertEqual(self.response_items_second[1][1], 'Title2')
        self.assertEqual(self.response_items_second[2][1], '2019-10-21T00:00:00Z')
        self.assertEqual(self.response_items_second[3][1], 'mental')
        self.assertEqual(self.response_items_second[4][1], None)
        self.assertEqual(self.response_items_second[5][1], '/media/test1')

class ArchiveSearchTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id, client_secret=self.oauth_client_secret)

    def setUpLoggedInEmployee(self):
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test')
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpContent(self):
        recipe = RecipeContent(title='Recipe', created_at=parser.parse('25/10/2019'), pillar='mental')
        article = ArticleContent(title='Article', created_at=parser.parse('21/10/2019'), pillar='physical')
        recipe.save()
        article.save()

    def test_get_published_content_by_pillar(self):
        self.setUpLoggedInEmployee()
        self.setUpContent()
        content = get_published_content_by_pillar(self.user, 'physical')
        self.assertEqual(content[0].title, 'Article')
        self.assertEqual(content[0].created_at.__str__(),'2019-10-20 23:00:00+00:00')
        self.assertEqual(content[0].pillar, 'physical')

    def test_get_published_content_by_content_type(self):
        self.setUpLoggedInEmployee()
        self.setUpContent()
        content = get_published_content_by_content_type('recipe')
        self.assertEqual(content[0].title, 'Recipe')
        self.assertEqual(content[0].created_at.__str__(), '2019-10-24 23:00:00+00:00')
        self.assertEqual(content[0].pillar, 'mental')
