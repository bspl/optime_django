from django import template

register = template.Library()


@register.filter(name='one_string_errors')
def one_string_errors(value):
    """
        Receives an error object in the format { 'field': ['error', 'error'], 'another_field': ['error'] } and truncates
        the errors into the format {'field': 'error string', 'another_field': 'error string'} where 'error string'
        is the list of errors concatenated into a string and separated by newline characters
    """
    result = {}

    for key in value:
        key_string = ''

        for key_value in value[key]:
            if key_string != '':
                key_string += '\n'
            key_string += str(key_value)

        result[key] = key_string

    return result



@register.simple_tag(takes_context=True, name='url_replace')
def url_replace(context, page):
    query = context['request'].GET.copy().urlencode()

    if '&page=' in query:
        url = query.rpartition('&page=')[0]  # equivalent to .split('page='), except more efficient
    else:
        url = query
    return f'{url}&page={page}'
