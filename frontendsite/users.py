from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.shortcuts import render
from django.views import View
from rest_framework import status

from optimeusers.models import OptimeUser, UserTypeStatus
from optimeusers.serializers import OptimeUserSerializer

from .views import get_user_and_greeting


class UserView(View):
    """
        get:
        serves the profile template. This is different for employers and employees

        post:
        updates the user object with the parameterised form data
    """

    def get(self, request):
        #get user first name and greeting
        user_data = get_user_and_greeting(request)
        # render profile page - different for employers and employees
        submitted = False
        if request.user.user_type == UserTypeStatus.Employer:
            return render(request, template_name='employer_dashboard/profile.html',
             context={'current_user': user_data.get('current_user'),
              'greet': user_data.get('current_user'),'submitted': submitted})
        return render(request, template_name='frontendsite/profile.html',
        context={'current_user': user_data.get('current_user'),
              'greet': user_data.get('greet'), 'submitted': submitted})

    # PATCH user- HTML forms don't support method=PATCH
    def post(self, request):
        # fetch current user
        user = OptimeUser.objects.get(pk=request.user.id)
        user_data = get_user_and_greeting(request)
        # Has email changed?
        submitted = True
        if request.POST.get('email') is not None:
            email = request.POST.get('email')

            # validate email
            try:
                validate_email(email)
            except ValidationError:
                return render(request, template_name='frontendsite/profile.html',
                              context={'error': {'email': ['Invalid email!']}},
                              status=status.HTTP_400_BAD_REQUEST)
            if OptimeUser.objects.filter(email__iexact=email).exists():
                return render(request, template_name='frontendsite/profile.html',
                              context={'error': {'email': ['A user with that email address already exists']}},
                              status=status.HTTP_409_CONFLICT)

        password = request.POST.get('password')

        if password is not None and password != '':
            # Perform set password via Django Auth to re-set password.
            password = request.POST.get('password')

            # Set password...
            user.set_password(password)

            # Save changes
            user.save()

        serializer = OptimeUserSerializer(user, data=request.POST, partial=True)
        if serializer.is_valid():
            serializer.save()

            user = OptimeUser.objects.get(pk=request.user.id)
            return render(request, template_name='frontendsite/profile.html',
                          context={'user': user, 'greet': user_data.get('greet'), 'current_user': user_data.get('current_user'), 'submitted': submitted })

        return render(request, template_name='frontendsite/profile.html',
                      context={'error': serializer.errors, 'greet': user_data.get('greet'), }, status=status.HTTP_400_BAD_REQUEST)
