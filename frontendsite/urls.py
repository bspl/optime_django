from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .content import AllContentView, RecipeContentView, ArticleContentView, NewsletterContentView, VideoContentView
from .employee_management import EmployeeView
from .login import RegistrationView, UserForgotPasswordView, LoginView, LogoutView, PasswordResetViewInherit
from .questionnaires import QuestionnaireView, CreateQuestionnaireView, QuestionnaireDetailView, \
    EmployeeQuestionnaireView, EmployeeQuestionnaireDetailView, QuestionnaireResponsesView, \
    QuestionnaireResponsesTextView, QuestionsSelfAssessmentDetailView, IntroQuestionnaireResponsesView
from .users import UserView
from .views import IndexView, EmployerIndexView, AwardsView, CookiePolicyView, TermsView, PrivacyPolicyView
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^employer/employees/$', login_required(EmployeeView.as_view()), name="frontendsite-employees"),
    url(r'^employer/questionnaires/$', login_required(QuestionnaireView.as_view()),
        name="frontendsite-employer-questionnaires"),
    url(r'^employer/questionnaires/(?P<pk>[0-9]+)/responses/$', login_required(QuestionnaireResponsesView.as_view()),
        name="frontendsite-employer-questionnaires-view-responses"),
    url(r'^employer/questionnaires/introductory/responses/$', login_required(IntroQuestionnaireResponsesView.as_view()),
        name="frontendsite-employer-intro-questionnaire-view-responses"),
    url(r'^employer/questionnaires/(?P<questionnaire_pk>[0-9]+)/responses/(?P<response_pk>[0-9]+)/$',
        login_required(QuestionnaireResponsesTextView.as_view()),
        name="frontendsite-employer-questionnaires-view-response-answers"),
    url(r'^employer/questionnaires/create/$', login_required(CreateQuestionnaireView.as_view()),
        name="frontendsite-questionnaire-create"),
    url(r'^employer/settings/$', login_required(UserView.as_view()), name="frontendsite-employer-profile"),
    url(r'^employer/questionnaires/edit/(?P<pk>[0-9]+)/$', login_required(QuestionnaireDetailView.as_view()),
        name="frontendsite-questionnaire-edit"),
    url(r'^employer/$', login_required(EmployerIndexView.as_view()), name="frontendsite-employer-index"),
    url(r'^archive/$', login_required(AllContentView.as_view()), name="frontendsite-archive"),
    url(r'^awards/$', login_required(AwardsView.as_view()), name="frontendsite-awards"),
    url(r'^recipe/(?P<pk>[0-9]+)/$', login_required(RecipeContentView.as_view()), name="frontendsite-recipe-detail"),
    url(r'^article/(?P<pk>[0-9]+)/$', login_required(ArticleContentView.as_view()), name="frontendsite-article-detail"),
    url(r'^newsletter/(?P<pk>[0-9]+)/$', login_required(NewsletterContentView.as_view()),
        name="frontendsite-newsletter-detail"),
    url(r'^video/(?P<pk>[0-9]+)/$', login_required(VideoContentView.as_view()), name="frontendsite-video-detail"),
    url(r'^register/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        RegistrationView.as_view(), name="frontendsite-register"),
    url(r'^forgot-password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        UserForgotPasswordView.as_view(), name="frontendsite-forgot-password"),
    url(r'^login/$', LoginView.as_view(), name="frontendsite-login"),
    url(r'^logout/$', LogoutView.as_view(), name="frontendsite-logout"),
    url(r'^cookies/$', CookiePolicyView.as_view(), name="frontendsite-cookies"),
    url(r'^terms/$', TermsView.as_view(), name="frontendsite-terms"),
    url(r'^privacy/$', PrivacyPolicyView.as_view(), name="frontendsite-privacy"),
    url(r'^profile/$', login_required(UserView.as_view()), name="frontendsite-profile"),
    url(r'^questionnaire/$', login_required(EmployeeQuestionnaireView.as_view()), name="frontendsite-questionnaires"),
    url(r'^questionnaire/(?P<pk>[0-9]+)/$', login_required(EmployeeQuestionnaireDetailView.as_view()),
        name="frontendsite-questionnaires-detail"),
    url(r'^questions/self-assessments$', login_required(QuestionsSelfAssessmentDetailView.as_view()),
        name="frontendsite-self-assessments"),
    url(r'^$', login_required(IndexView.as_view()), name="frontendsite-index"),
    # reset password
    url(r'^password_reset/$', PasswordResetViewInherit.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]
