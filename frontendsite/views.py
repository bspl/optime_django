import pytz
from datetime import datetime, timedelta

from dateutil import parser
from django.shortcuts import render, redirect
from django.views import View
from django.views.decorators.vary import vary_on_cookie
from rest_framework import status
from content.views import get_published_content_by_pillar, \
    get_published_content_by_content_type, get_published_content

from companies.models import CompanyAwards
from content.views import get_published_content
from content.models import RecipeContent, ArticleContent, NewsletterContent
from content.views import get_goals_video, recipe_meets_dietary_reqs
from goals.models import UserGoal
from optimeusers.models import UserTypeStatus, UserCompanyAward
from reports.views import get_radar_chart_data, get_employee_engagement_data, get_employee_wellbeing_data


def render_404(request):
    """renders 404 error page"""
    return render(request, template_name='404.html', status=status.HTTP_404_NOT_FOUND)


def _check_for_redirect(user, expected_type):
    """
        Auxiliary function checks if user should be redirected (if they're accessing the wrong index)
        :param user: the user to test
        :param expected_type: the user_type expected for the endpoint they're accessing
        :return: the name of the url to redirect them to, or None if they are accessing the correct endpoint
    """
    # if I'm an admin, redirect me to the admin panel
    if user.is_superuser:
        return 'admin:index'

    # if I'm an employer, render me the employer index
    if expected_type != UserTypeStatus.Employer and user.user_type == UserTypeStatus.Employer:
        return 'frontendsite-employer-index'

    # otherwise render me the employee index
    if expected_type != UserTypeStatus.Employee and user.user_type == UserTypeStatus.Employee:
        return 'frontendsite-index'

    return None

# set User name and greetings method
def get_user_and_greeting(request):
    # greeting for user
    now = datetime.now()
    hour = now.hour
    if request.session.get('user_timezone'):    
        tz = pytz.timezone(request.session.get('user_timezone'))
        now = datetime.now(tz)
        hour = now.hour

    if hour < 12:
        greeting = "Good morning"
    elif hour < 18:
        greeting = "Good afternoon"
    else:
        greeting = "Good night"
    print(' request user and  greet>>>>>>>>', request.user)
    return {
        'current_user': request.user,
        'greet': greeting
    }



class EmployerIndexView(View):
    """
        get:
        renders employer index page with the reports in context. Optional start_date and end_date in the URL to filter
        reports by date
    """

    @vary_on_cookie
    def get(self, request):
        redirect_url = _check_for_redirect(request.user, UserTypeStatus.Employer)
        if redirect_url is not None:
            return redirect(redirect_url)

        # fetch start_date and end_date query params
        start_date_url = request.GET.get('start_date')
        end_date_url = request.GET.get('end_date')

        # default to 1 year ago
        now = datetime.now()
        start_date = now - timedelta(weeks=52)
        end_date = now

        # parse dates
        try:
            if start_date_url is not None:
                # 'all' is a special value
                if start_date_url == 'all':
                    # None returns all dates
                    start_date = None
                else:
                    start_date = parser.parse(start_date_url)

        except ValueError:
            return render(request, template_name='employer_dashboard/index.html',
                          context={'error': {'start_date': ['Invalid start_date passed!']}},
                          status=status.HTTP_400_BAD_REQUEST)

        try:
            if end_date_url is not None:
                # 'all' is a special value
                if end_date_url == 'all':
                    # None returns all dates
                    end_date = None
                else:
                    end_date = parser.parse(end_date_url)

        except ValueError:
            return render(request, template_name='employer_dashboard/index.html',
                          context={'error': {'end_date': ['Invalid end_date passed!']}},
                          status=status.HTTP_400_BAD_REQUEST)

        # build reports
        # get employee engagement data
        engagement_data = get_employee_engagement_data(request.user, start_date, end_date)

        # get employee wellbeing data
        wellbeing_data = get_employee_wellbeing_data(request.user, start_date, end_date)

        return render(request, template_name='employer_dashboard/index.html',
                      context={'start_date': start_date, 'end_date': end_date, 'wellbeing_data': wellbeing_data,
                               'engagement_data': engagement_data})


class IndexView(View):
    """
        get:
        renders employee index page with the relevant content in the context, and the radar chart for their progress
    """

    def get(self, request):
        redirect_url = _check_for_redirect(request.user, UserTypeStatus.Employee)
        if redirect_url is not None:
            return redirect(redirect_url)

        date = datetime.now()

        # get content list
        content = list()

        # Homepage content _should_ be...
        # one exercise, one recipe, latest news letter

        # Try and serve the user an exercise for their relevant occupational activity level, if one exists...
        # otherwise fall back to a generic exercise.
        exercise_articles = ArticleContent.objects.filter(publish_date__lte=date,
                                                          occupational_activity=request.user.occupational_activity).order_by(
            '-publish_date')[:1]
        if exercise_articles.count() > 0:
            exercise = exercise_articles.first()
            content.append(exercise)
        else:
            exercise_articles = ArticleContent.objects.filter(publish_date__lte=date).order_by(
                '-publish_date')[:1]
            if exercise_articles.count() > 0:
                exercise = exercise_articles.first()
                content.append(exercise)

        # TODO: Improve efficiency of this as recipe archive grows in future!
        recipes = RecipeContent.objects.filter(publish_date__lte=date).order_by('-publish_date')

        found_relevant_recipe = False
        # filter recipe content by user's dietary requirements
        for recipe in recipes:
            if recipe_meets_dietary_reqs(recipe, request.user):
                content.append(recipe)
                found_relevant_recipe = True
                break
            else:
                continue

        if not found_relevant_recipe:
            # default to most recent recipe regardless of dietary requirements...
            recipes = RecipeContent.objects.filter(publish_date__lte=date).order_by('-publish_date')[:1]
            most_recent_recipe = recipes.first()
            content.append(most_recent_recipe)

        # Does a published company newsletter exist? If not, fall back to site-wide newsletter.
        if NewsletterContent.objects.filter(publish_date__lte=date, company=request.user.company).exists():
            newsletters = NewsletterContent.objects.filter(publish_date__lte=date,
                                                           company=request.user.company).order_by('-publish_date')[:1]
        else:
            newsletters = NewsletterContent.objects.filter(publish_date__lte=date).order_by('-publish_date')[:1]

        if newsletters.count() > 0:
            newsletter = newsletters.first()
            content.append(newsletter)

        # get the goals video for this month
        goals_video = get_goals_video()

        # get my active goal
        try:
            goal = UserGoal.objects.get(user=request.user, is_active=True)
        except UserGoal.DoesNotExist:
            goal = None

        # get my radar chart
        radar_chart_data = get_radar_chart_data(request.user)

        # get my awards
        current_user_awards = UserCompanyAward.objects.filter(user=request.user).order_by('created_at')
        
        # get username and greeting
        user_data = get_user_and_greeting(request)
        
        # get all content data
        get_all_videos = get_published_content(request.user)
        # filter for videos 
        form_content_type = request.GET.get("type", 'all')
        form_pillar = request.GET.get("pillar", 'all')
        # render index page

        if request.GET.get('all_data') == 'true':
            form_all_data = True
        else:
            form_all_data = False

        if form_content_type == 'all' and form_pillar != 'all':
            get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
        elif form_content_type != 'all' and form_pillar == 'all':
            get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
        elif form_content_type == 'all' and form_pillar == 'all':
            get_all_videos = get_published_content(request.user)
        else:
            types = get_published_content_by_content_type(request.user, form_content_type)
            pillar = get_published_content_by_pillar(request.user, form_pillar)

            if types is None:
                types = []

            if pillar is None:
                pillar = None

            get_all_videos = list(set(types) & set(pillar))
        print(">>>>>>>>>>>>>>>", form_pillar)

        return render(request, template_name='frontendsite/index.html',
                      context={'content': content, 'goal': goal, 'goals_video': goals_video,
                                'greet': user_data.get('greet'),
                                'current_user': user_data.get('current_user'),
                                'all_content': get_all_videos, 'current_content_type': form_content_type,
                                'current_pillar': form_pillar,
                                'radar_chart_data': radar_chart_data, 'my_awards': current_user_awards})


class TermsView(View):
    """
        get:
        renders terms of use page.
    """

    def get(self, request):
        # get username and greeting
        user_data = get_user_and_greeting(request)
        return render(request, 'frontendsite/terms.html', context={
                                'greet': user_data.get('greet'),
                                'current_user': user_data.get('current_user')
                                })


class CookiePolicyView(View):
    """
        get:
        renders cookie policy page.
    """

    def get(self, request):
        # get username and greeting
        user_data = get_user_and_greeting(request)
        return render(request, 'frontendsite/cookie_policy.html', context={
                                'greet': user_data.get('greet'),
                                'current_user': user_data.get('current_user')
                                })


class PrivacyPolicyView(View):
    """
        get:
        renders Privacy policy page.
    """

    def get(self, request):
        # get username and greeting
        user_data = get_user_and_greeting(request)
        return render(request, 'frontendsite/privacy_policy.html', context={
                                'greet': user_data.get('greet'),
                                'current_user': user_data.get('current_user')
                                })


class AwardsView(View):
    """
    get:
    renders awards from company awards in get request
    """

    def get(self, request):
        company_awards = CompanyAwards.objects.filter(company=request.user.company)
        awards = [c.award for c in company_awards]
        latest_winners = []
        for award in awards:
            if UserCompanyAward.objects.filter(company_award__award=award).order_by('created_at').last() is not None:
                latest_winners.append(
                    UserCompanyAward.objects.filter(company_award__award=award).order_by('created_at').last())

        current_awards_user = UserCompanyAward.objects.filter(user=request.user).order_by('created_at')

        # get username and greeting
        user_data = get_user_and_greeting(request)

        return render(request, template_name='frontendsite/awards.html',
                      context={'company_awards': awards, 'current_awards': current_awards_user,
                               'latest_awards': latest_winners,
                               'greet': user_data.get('greet'),
                               'current_user': user_data.get('current_user'),
                               })
