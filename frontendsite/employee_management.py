import uuid

from django.core.exceptions import ValidationError
from django.core.paginator import Paginator
from django.core.validators import validate_email
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.views import APIView

from awards.models import Awards
from companies.models import CompanyAwards
from optime.settings import PAGINATION_RESULTS_PER_PAGE
from optimeusers.models import OptimeUser, UserTypeStatus, UserCompanyAward
from optimeusers.tasks import send_invite_task
from optimeusers.views import get_password_link
from questionnaires.permissions import EmployerPermission


def _get_paginated_employees(request):
    """
        Auxiliary function for getting a paginated list of employees
        :param request: the web request object, containing the requested page in query_params (optionally)
        :return: the requested page of employees
    """
    # get employees relating to this company
    employees = OptimeUser.objects.filter(company_id=request.user.company.pk,
                                          user_type=UserTypeStatus.Employee).order_by('-is_active')
    print("??????????????")
    # requested page
    page = request.query_params.get('page')

    # default to page 1
    if page is None:
        page = 1

    # apply pagination to results
    paginator = Paginator(employees, PAGINATION_RESULTS_PER_PAGE)
    return paginator.get_page(page)


class EmployeeView(APIView):
    """
       get:
       serves the employees template with paginated employees

       post:
       receives an email address and creates a new employee object with this email, then sends an email to the user
       asking them to confirm. If the user has already been invited but they haven't confirmed, it resends the invite
    """
    permission_classes = (EmployerPermission,)

    def get(self, request):
        employees = _get_paginated_employees(request)

        # pagination data for the context
        current_page = employees.number
        num_pages = employees.paginator.num_pages

        user_comp_awards = UserCompanyAward.objects.filter(company_award__company=request.user.company)
        comp_awards = CompanyAwards.objects.filter(company=request.user.company)

        award_to_user = {}
        for award in user_comp_awards:
            if award.company_award.award is not None:
                award_to_user[award.company_award.award] = award.user

        for award in comp_awards:
            if award.award not in award_to_user and award.award is not None:
                award_to_user[award.award] = None

        return render(request, template_name='employer_dashboard/employees.html',
                      context={'employees': employees, 'current_page': current_page, 'num_pages': num_pages,
                               'pages': range(num_pages + 1), 'awards': award_to_user})

    # inviting a new employee
    def post(self, request):
        email = request.POST.get('email')
        print(">>>>>>>>>>>>>>>>", email)
        # validate email
        try:
            validate_email(email)
        except ValidationError:
            employees = _get_paginated_employees(request)

            # pagination data for the context
            current_page = employees.number
            num_pages = employees.paginator.num_pages

            return render(request, template_name='employer_dashboard/employees.html',
                          context={'employees': employees, 'current_page': current_page, 'num_pages': num_pages,
                                   'pages': range(num_pages + 1), 'error': {'email': ['Invalid email!']}},
                          status=status.HTTP_400_BAD_REQUEST)

        user = None

        if OptimeUser.objects.filter(email__iexact=email).exists():
            user = OptimeUser.objects.get(email__iexact=email)

            # if user is activated, reject the request
            if user.email_confirmed is True:
                employees = _get_paginated_employees(request)

                # pagination data for the context
                current_page = employees.number
                num_pages = employees.paginator.num_pages

                return render(request, template_name='employer_dashboard/employees.html',
                              context={'employees': employees, 'current_page': current_page, 'num_pages': num_pages,
                                       'pages': range(num_pages + 1),
                                       'error': {'email': ['A user with that email address already exists']}},
                              status=status.HTTP_409_CONFLICT)

        if user is None:
            # create the basics
            user = OptimeUser()
            user.email = email
            user.company = request.user.company
            user.user_type = UserTypeStatus.Employee
            user.username = str(uuid.uuid4())

            # generate a long password
            user.set_password(uuid.uuid4())
            user.save()

        # generate a reset password link
        link = get_password_link('frontendsite-register', user)

        # email this to the employee
        send_invite_task.apply_async((user.pk, link))

        # return success
        return redirect('frontendsite-employees')
