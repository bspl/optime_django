from django.core.paginator import Paginator
from django.shortcuts import render
from django.views import View
from rest_framework import status

from content.models import Content, RecipeContent, ArticleContent, NewsletterContent, VideoContent
from content.views import get_published_content_by_pillar, \
    get_published_content_by_content_type, get_published_content
from optime.settings import PAGINATION_RESULTS_PER_PAGE
from .views import get_user_and_greeting


def serve_404(request):
    """Auxiliary function to serve a 404 error page"""
    return render(request, template_name='404.html', status=status.HTTP_404_NOT_FOUND)


class AllContentView(View):
    """
       get:
       returns all content for a parameterised month. Serving the archive template
    """

    def get(self, request):
        form_content_type = request.GET.get("type", 'all')
        form_pillar = request.GET.get("pillar", 'all')

        if request.GET.get('all_data') == 'true':
            form_all_data = True
        else:
            form_all_data = False

        # check all combinations of inputs if they're valid and return the appropriate response
        if form_content_type == 'all' and form_pillar != 'all':
            content = get_published_content_by_pillar(request.user, form_pillar)
        elif form_content_type != 'all' and form_pillar == 'all':
            content = get_published_content_by_content_type(request.user, form_content_type)
        elif form_content_type == 'all' and form_pillar == 'all':
            content = get_published_content(request.user)
        else:
            types = get_published_content_by_content_type(request.user, form_content_type)
            pillar = get_published_content_by_pillar(request.user, form_pillar)

            if types is None:
                types = []

            if pillar is None:
                pillar = None

            content = list(set(types) & set(pillar))

        if content is not None and len(content) > PAGINATION_RESULTS_PER_PAGE:
            paginator = Paginator(content, PAGINATION_RESULTS_PER_PAGE)
            page = request.GET.get('page', 1)
            current_content = paginator.get_page(page)
            current_page = current_content.number
            num_pages = current_content.paginator.num_pages
        else:
            content = content
            page = 1
            current_content = content
            current_page = 1
            num_pages = 1

        #get user first name and greeting
        user_data = get_user_and_greeting(request)


        return render(request, template_name='frontendsite/archive.html',
                      context={'content': current_content, 'current_page': current_page, 'num_pages': num_pages,
                               'pages': range(num_pages + 1), 'current_pillar': form_pillar,
                               'current_content_type': form_content_type,
                               'current_user': user_data.get('current_user'),
                               'greet': user_data.get('greet'),
                               'date_all': form_all_data})


class RecipeContentView(View):
    """
       get:
       returns recipe template rendering the parameterised recipe, or 404 if it doesn't exist
    """

    def get(self, request, pk):
        try:
            content = RecipeContent.objects.get(pk=pk)
            #get user first name and greeting
            user_data = get_user_and_greeting(request)
             # get all content data
            get_all_videos = get_published_content(request.user)
            # filter for videos 
            form_content_type = request.GET.get("type", 'all')
            form_pillar = request.GET.get("pillar", 'all')
            # render index page

            if request.GET.get('all_data') == 'true':
                form_all_data = True
            else:
                form_all_data = False

            if form_content_type == 'all' and form_pillar != 'all':
                get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
            elif form_content_type != 'all' and form_pillar == 'all':
                get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
            elif form_content_type == 'all' and form_pillar == 'all':
                get_all_videos = get_published_content(request.user)
            else:
                types = get_published_content_by_content_type(request.user, form_content_type)
                pillar = get_published_content_by_pillar(request.user, form_pillar)

                if types is None:
                    types = []

                if pillar is None:
                    pillar = None

                get_all_videos = list(set(types) & set(pillar))
            return render(request, template_name='frontendsite/content/recipe.html',
                          context={'content': content,'current_user': user_data.get('current_user'),
                               'greet': user_data.get('greet'),'all_content': get_all_videos,'current_content_type': form_content_type,
                               'current_pillar': form_pillar,})
        except Content.DoesNotExist:
            return serve_404(request)


class ArticleContentView(View):
    """
       get:
       returns article template rendering the parameterised object, or 404 if it doesn't exist
    """

    def get(self, request, pk):
        try:
            content = ArticleContent.objects.get(pk=pk)
            #get user first name and greeting
            user_data = get_user_and_greeting(request)
             # get all content data
            get_all_videos = get_published_content(request.user)
            # filter for videos 
            form_content_type = request.GET.get("type", 'all')
            form_pillar = request.GET.get("pillar", 'all')
            # render index page

            if request.GET.get('all_data') == 'true':
                form_all_data = True
            else:
                form_all_data = False

            if form_content_type == 'all' and form_pillar != 'all':
                get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
            elif form_content_type != 'all' and form_pillar == 'all':
                get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
            elif form_content_type == 'all' and form_pillar == 'all':
                get_all_videos = get_published_content(request.user)
            else:
                types = get_published_content_by_content_type(request.user, form_content_type)
                pillar = get_published_content_by_pillar(request.user, form_pillar)

                if types is None:
                    types = []

                if pillar is None:
                    pillar = None

                get_all_videos = list(set(types) & set(pillar))
            return render(request, template_name='frontendsite/content/article.html',
                          context={'content': content,'current_user': user_data.get('current_user'),
                               'greet': user_data.get('greet'),'all_content': get_all_videos,'current_content_type': form_content_type,
                               'current_pillar': form_pillar,})

        except Content.DoesNotExist:
            return serve_404(request)


class NewsletterContentView(View):
    """
       get:
       returns newsletter template rendering the parameterised object, or 404 if it doesn't exist
    """

    def get(self, request, pk):
        try:
            content = NewsletterContent.objects.get(pk=pk)
            user_data = get_user_and_greeting(request)
             # get all content data
            get_all_videos = get_published_content(request.user)
            # filter for videos 
            form_content_type = request.GET.get("type", 'all')
            form_pillar = request.GET.get("pillar", 'all')
            # render index page

            if request.GET.get('all_data') == 'true':
                form_all_data = True
            else:
                form_all_data = False

            if form_content_type == 'all' and form_pillar != 'all':
                get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
            elif form_content_type != 'all' and form_pillar == 'all':
                get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
            elif form_content_type == 'all' and form_pillar == 'all':
                get_all_videos = get_published_content(request.user)
            else:
                types = get_published_content_by_content_type(request.user, form_content_type)
                pillar = get_published_content_by_pillar(request.user, form_pillar)

                if types is None:
                    types = []

                if pillar is None:
                    pillar = None

                get_all_videos = list(set(types) & set(pillar))

            return render(request, template_name='frontendsite/content/newsletter.html',
                          context={'content': content,'all_content': get_all_videos,'current_content_type': form_content_type,
                               'current_pillar': form_pillar,'current_user': user_data.get('current_user'),'greet': user_data.get('greet'),})

        except Content.DoesNotExist:
            return serve_404(request)


class VideoContentView(View):
    """
       get:
       returns video template rendering the parameterised object, or 404 if it doesn't exist
    """

    def get(self, request, pk):
        try:
            content = VideoContent.objects.get(pk=pk)   
            #get user first name and greeting
            user_data = get_user_and_greeting(request)  
              # get all content data
            get_all_videos = get_published_content(request.user)
            # filter for videos 
            form_content_type = request.GET.get("type", 'all')
            form_pillar = request.GET.get("pillar", 'all')
            # render index page

            if request.GET.get('all_data') == 'true':
                form_all_data = True
            else:
                form_all_data = False

            if form_content_type == 'all' and form_pillar != 'all':
                get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
            elif form_content_type != 'all' and form_pillar == 'all':
                get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
            elif form_content_type == 'all' and form_pillar == 'all':
                get_all_videos = get_published_content(request.user)
            else:
                types = get_published_content_by_content_type(request.user, form_content_type)
                pillar = get_published_content_by_pillar(request.user, form_pillar)

                if types is None:
                    types = []

                if pillar is None:
                    pillar = None

                get_all_videos = list(set(types) & set(pillar))
            return render(request, template_name='frontendsite/content/video.html',
                          context={'content': content,'current_user': user_data.get('current_user'),
                               'greet': user_data.get('greet'),'all_content': get_all_videos,'current_content_type': form_content_type,
                               'current_pillar': form_pillar,})

        except Content.DoesNotExist:
            return serve_404(request)
