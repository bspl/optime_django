# Create your tests here.
from django.test import TestCase

from frontendsite.questionnaires import calculate_results, get_question_response_type, count_answers
from questionnaires.models import QuestionTypeStatus, BooleanQuestionnaireQuestionResponse, Question


class QuestionnaireResponsesTests(TestCase):

    def test_get_answer_percentage(self):
        self.assertEqual(calculate_results(3, {True:3}), {True:100.00})

    def test_get_question_response_type(self):
        self.assertEqual(get_question_response_type(QuestionTypeStatus.Boolean), BooleanQuestionnaireQuestionResponse)

    def test_count_answers(self):
        response = BooleanQuestionnaireQuestionResponse(questionnaire_response=None, questionnaire_question=None)
        response.response = True
        question = Question()
        question.response_type = QuestionTypeStatus.Boolean
        self.assertEqual(count_answers([response], question), {True: 1,False: 0})
