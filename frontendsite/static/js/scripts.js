
$(document).ready(function () {
        let pageNum = 1;

        // Ajax Control
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                ''

                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }

                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });

        // Grabs the svg and adds the company colour to the style element
        // Currently doesnt work live. probably due to cors issue as the svg is received from a different domain
        var svgHolderAll = document.querySelectorAll('object.dynamic-svg');
        svgHolderAll.forEach(function (svgHolder) {
            svgHolder.onload = function () {
                const svgDocument = svgHolder.contentDocument;
                style = ':root {--secondary:' + localStorage.getItem("secondary_colour") + '}';
                if (svgDocument.querySelector('defs')) {
                    const svgElem = svgDocument.querySelector('defs');
                    const svgStyle = svgElem.querySelector('style');
                    svgStyle.prepend(style)
                }
            };
        });

        // Publishing Questionnaires
        $(".publishBtn").click(function () {
            let theId = $(this).attr('data-id');
            $.ajax({
                url: '/api/questionnaires/publish/',
                data: {
                    id: theId
                },
                success: function (data) {
                    location.reload();

                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });

        let theId;
        // Deleting Questionnaires
        $(".deleteModalBtn").click(function () {
            theId = $(this).attr('data-id');
        });

        $("#deleteBtn").click(function () {
            $.ajax({
                url: '/api/questionnaires/' + theId + '/',
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'DELETE'
            });
        });
        // Deleting Employees
        $("#deleteEmployeeBtn").click(function () {
            $.ajax({
                url: '/api/users/' + theId + '/',
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'DELETE'
            });
        });

        // Sends a email to the employee to reset their password
        $(".resetEmployeePassword").click(function () {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '/api/users/forgot/' + id + '/',
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });

        /* Survey Page */

        surveyPage();
        let surveyResponse = [];

        function surveyPage() {
            $(".surveyQuestion").hide();
            $('#surveyPg' + pageNum).show();
            let lastQuestion = $('.lastQuestion').attr('data-index');
            // setting progress bar
            let percentage = Math.round(((pageNum / lastQuestion) * 100));
            $('#surveyProgress').css('width', percentage + '%').attr('aria-valuenow', percentage);
        }

        $('.nextQuestion').click(function () {
            let id = $('#surveyPg' + pageNum).attr('data-id');
            let response = {};
            response.id = id;
            response.response_type = $('#surveyPg' + pageNum).attr('data-type');
            // Loop though booleans to get data
            if (response.response_type === 'boolean') {
                response.response = $('.question' + id + 'Input').is(":checked");
            } else {
                response.response = $('.question' + id + 'Input').val();
            }
            // Add to response
            surveyResponse.push(response);
            $("#surveyResponse").val(JSON.stringify(surveyResponse));
            pageNum++;
            surveyPage();
        });

        $("#questionnaireForm").on('submit', function () {
            let id = $('#surveyPg' + pageNum).attr('data-id');
            let response = {};
            response.id = id;
            response.response_type = $('#surveyPg' + pageNum).attr('data-type');
            if ($('.question' + id + 'Input').val() === 'on') {
                response.response = 'true';
            } else {
                response.response = $('.question' + id + 'Input').val();
            }
            surveyResponse.push(response);
            $("#surveyResponse").val(JSON.stringify(surveyResponse));
            $(this)[0].submit();
        });

        // Changing Colors Randomly (for Testing)
        function codeInput(cb) {
            var input = '';
            var key = '38384040373937396665';
            document.addEventListener('keydown', function (e) {
                input += ("" + e.keyCode);
                if (input === key) {
                    return cb();
                }
                if (!key.indexOf(input)) return;
                input = ("" + e.keyCode);
            });
        }

        codeInput(function () {
            alert('Disco!');
            setInterval(function () {
                setNewColors();
            }, 500);
        });

        function getRandomColor() {
            const letters = '0123456789ABCDEF';
            let color = '#';
            for (let i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function setNewColors() {
            primaryColor = getRandomColor();
            primaryAccentColor = getRandomColor();
            secondaryColor = getRandomColor();
            secondaryRgb = hex2rgb(secondaryColor);
            primaryAccentRgb = hex2rgb(primaryAccentColor);
            document.body.style.setProperty('--primary', primaryColor);
            document.body.style.setProperty('--primary-accent', primaryAccentColor);
            document.body.style.setProperty('--secondary', secondaryColor);
            document.body.style.setProperty('--pulse', 'rgba(' + secondaryRgb + ',0.4)');
            document.body.style.setProperty('--pulse-zero', 'rgba(' + secondaryRgb + ',0)');
        }

        // Variable colors
        let primaryColor = '#25428A';
        let primaryAccentColor = '#15327A';
        let secondaryColor = '#00AEEF';

        if (localStorage.getItem("primary_colour") !== null) {
            primaryColor = localStorage.getItem("primary_colour");
        }

        if (localStorage.getItem("primary_accent_colour") !== null) {
            primaryAccentColor = localStorage.getItem("primary_accent_colour");
        }

        if (localStorage.getItem("secondary_colour") !== null) {
            secondaryColor = localStorage.getItem("secondary_colour");
        }

        // This is a back up for inline styles and used for colour control within the scripts
        let secondaryRgb = hex2rgb(secondaryColor);
        let primaryAccentRgb = hex2rgb(primaryAccentColor);
        document.body.style.setProperty('--pulse', 'rgba(' + secondaryRgb + ',0.4)');
        document.body.style.setProperty('--pulse-zero', 'rgba(' + secondaryRgb + ',0)');
        // Theme Color
        let m = document.createElement('meta');
        m.name = 'theme-color';
        m.content = localStorage.getItem("primary_colour");
        document.head.appendChild(m);

        function hex2rgb(hex) {
            // long version
            r = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
            if (r) {
                return r.slice(1, 4).map(function (x) {
                    return parseInt(x, 16);
                });
            }
            // short version
            r = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
            if (r) {
                return r.slice(1, 4).map(function (x) {
                    return 0x11 * parseInt(x, 16);
                });
            }
            return null;
        }

        // Video + Goals
        $("#goals").hide();
        $("#video").hide();
        $(".backBtn").hide();
        $("#goalSelect").hide();
        $('#poster').click(function () {
            $("#posterImg").hide();
            $("#video").show().trigger('play');
            if ($(window).width() < 990) {
                $("#leftSide").hide('slow')
            }
        });


        $('.addVideoGoal').click(function () {
            let videoGoalId = $(this).attr('data-id');
            $.ajax({
                url: '/api/goals/video/',
                data: {
                    id: videoGoalId
                },
                success: function (data) {
                    location.reload();

                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });

        // Display congratulations on goals completions
        $('#congratulations').hide();
        goalCheck();

        function goalCheck() {
            let allChecked = true;
            $('.goalCheck').each(function () {
                console.log($(this).attr('data-active'));
                if ($(this).attr('data-active') === 'False') {
                    allChecked = false;
                }
            });
            if (allChecked) {
                $('#congratulations').show();
            } else {
                $('#congratulations').hide();
            }
        }

        $(".goalCheck").click(function () {
            let complete;
            if ($(this).attr('data-active') === 'True') {
                complete = 'False';
            } else {
                complete = 'True'
            }
            let id = $(this).attr('data-id');
            $.ajax({
                url: '/api/goals/task/' + id + '/',
                data: {
                    is_complete: complete
                },
                success: function (data) {
                    // location.reload();
                    if (complete === 'True') {
                        $('#goal' + id).attr('data-active', 'True');
                    } else {
                        $('#goal' + id).attr('data-active', 'False');
                    }
                    goalCheck();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });

        });


        if ($('#registrationForm').length) {
            $(document).on("keydown", "form", function (event) {
                let enterKeyCode = 13;
                let lastRegistrationPage = 4;
                if (event.keyCode === enterKeyCode) {
                    if (pageNum !== lastRegistrationPage) {
                        pageNum = pageNum + 1;
                        checkCurrentPage();
                        return event.key != "Enter";
                    }
                }
            });
        }

        // Control Side Menu
        checkWidth();

        $(window).resize(function () {
            checkWidth();
        });

        function checkWidth() {
            if ($(window).width() < 990) {

                $("#sidebar-wrapper").addClass("hidden");
                $("#wrapper").addClass("hidden");
                $("#page-content-wrapper").addClass("hidden");
                $("#menu").addClass("hidden").removeClass("is-active");
                $('#selfAssessment').click(function () {
                    hideNav();
                });
            } else {

                $("#sidebar-wrapper").removeClass("hidden");
                $("#wrapper").removeClass("hidden");
                $("#page-content-wrapper").removeClass("hidden");
                $("#menu").removeClass("hidden").addClass("is-active");
            }
        }

        $('#menu').click(function () {
            $("#sidebar-wrapper").show();
            hideNav();
        });
        $('#logoutmobile').click(function () {
           $('#menu').click(); 
        });

        


        

        function hideNav() {
            $("#sidebar-wrapper").toggleClass("hidden");
            $("#wrapper").toggleClass("hidden");
            $("#page-content-wrapper").toggleClass("hidden");
            $("#menu").toggleClass("hidden is-active");
        }

        // On-boarding Imperial Toggle

        //Height
        //1 inch = 2.54cm
        let inchCm = 2.54;
        let cmInch = 0.393701;
        $('#cmBtn').click(function () {
            if ($('#heightInput').attr('data-input') === 'inch') {
                let val = $('#heightInput').val();
                if (val.length > 0) {
                    $('#heightInput').val(Math.round(val * inchCm));
                }
                $('#heightInput').attr('data-input', 'cm');
                $(this).addClass('btn-light').removeClass('btn-outline-light');
                $('#inchBtn').addClass('btn-outline-light').removeClass('btn-light');
            }
        });
        $('#inchBtn').click(function () {
            if ($('#heightInput').attr('data-input') === 'cm') {
                let val = $('#heightInput').val();
                if (val.length > 0) {
                    $('#heightInput').val(Math.round(val * cmInch));
                }
                $('#heightInput').attr('data-input', 'inch');
                $(this).addClass('btn-light').removeClass('btn-outline-light');
                $('#cmBtn').addClass('btn-outline-light').removeClass('btn-light');
            }
        });

        //Weight
        // 1kg =  2.20462 lbs
        let kgLbs = 2.20462;
        let lbsKg = 0.453592;
        $('#kgBtn').click(function () {
            if ($('#weightInput').attr('data-input') === 'lbs') {
                let val = $('#weightInput').val();
                if (val.length > 0) {
                    $('#weightInput').val(round((val * lbsKg), 1));
                }
                $('#weightInput').attr('data-input', 'kg');
                $(this).addClass('btn-light').removeClass('btn-outline-light');
                $('#lbsBtn').addClass('btn-outline-light').removeClass('btn-light');
            }
        });

        $('#lbsBtn').click(function () {
            if ($('#weightInput').attr('data-input') === 'kg') {
                let val = $('#weightInput').val();
                if (val.length > 0) {
                    $('#weightInput').val(round((val * kgLbs), 1));
                }
                $('#weightInput').attr('data-input', 'lbs');
                $(this).addClass('btn-light').removeClass('btn-outline-light');
                $('#kgBtn').addClass('btn-outline-light').removeClass('btn-light');
            }
        });

        $("#registrationForm").on('submit', function () {
            if ($('#weightInput').attr('data-input') === 'lbs') {
                let val = $('#weightInput').val();
                $('#weightInput').val(round((val * lbsKg), 1));
            }
            if ($('#heightInput').attr('data-input') === 'inch') {
                let val = $('#heightInput').val();
                $('#heightInput').val(round((val * inchCm), 1));
            }
            $(this)[0].submit();
        });

        // on form submit if height is in inches convert to cm
        // if weight is in lbs convert to kg

        // on-boarding pagination
        function round(x, n) {
            return Math.round(x * Math.pow(10, n)) / Math.pow(10, n)
        }

        checkCurrentPage();

        function checkCurrentPage() {
            let totalOnboardingPages = [1, 2, 3, 4];
            let index = totalOnboardingPages.indexOf(pageNum);
            if (index > -1) {
                totalOnboardingPages.splice(index, 1);
            }
            $('#pg' + pageNum).show();
            $('#img' + pageNum).show();
            $('#pag' + pageNum).css("fill", "white");
            for (i = 0; i < totalOnboardingPages.length; i++) {
                $('#pg' + totalOnboardingPages[i]).hide();
                $('#img' + totalOnboardingPages[i]).hide();
                $('#pag' + totalOnboardingPages[i]).css("fill", "var(--primary)");
            }
            if (pageNum === 1) {
                $('#backBtn').hide();
                //$('#nextBtn').html('<a class="wellbeing000" href="javascript:void">Start my wellbeing</a>')
                $('#nextBtn').html('Start my wellbeing journey')
                $('#nextBtn').addClass('startmywellbeig')

            } else {
                $('#backBtn').show();
            }
            $('#pillars').hide();
            if (pageNum === 2) {
                $('#pillars').hide();
                $('#nextBtn').html('Create')
                $('#nextBtn').removeClass('startmywellbeig')
                $('#backBtn').html('Back')
                setTimeout(function () {
                    $('#pillars').show();
                }, 10);
            } else if (pageNum === 3) {
                $('#pillarsReverse').hide();
                $('#nextBtn').html('Submit')
                $('#backBtn').html('Back')
                 $('#nextBtn').removeClass('startmywellbeig')
                $('#pillars').hide();
                setTimeout(function () {
                    $('#pillars').show();
                }, 10);
            } else {
                $('#pillarsReverse').hide();
                $('#pillars').show();
            }
            if (pageNum === 4) {
                $('#submitBtn').show();
                $('#nextBtn').hide();
            } else {
                $('#submitBtn').hide();
                $('#nextBtn').show();
            }
        }

        $('#invalidName').hide();
        $('#nextBtn').click(function () {
            pageNum++;
            checkPassword();
            if (pageNum === 4) {
                pageNum = 3;
                if ($("#firstName").val().length > 0 && $("#lastName").val().length > 0) {
                    pageNum = 4;
                    $('#invalidName').hide();
                    checkCurrentPage();
                } else {
                    $('#invalidName').show();
                }
            } else {
                checkCurrentPage();
            }
        });

        $('.pagCircle').click(function () {
            pageNum = parseInt($(this).attr("data-number"));
            checkPassword();
            checkCurrentPage();
        });

        $('#backBtn').click(function () {
            pageNum--;
            checkPassword();
            checkCurrentPage();
        });

        // Password Validation
        $('#invalidPassword').hide();

        let passwordFullStrength = false;

        function checkPassword() {
            if (pageNum > 2) {
                if (passwordFullStrength === false) {
                    pageNum = 2;
                    $('#invalidPassword').show();
                } else {
                    $('#invalidPassword').hide();
                }
            }
        }

        $("#password").keyup(function () {
            checkPasswordStrength();
        });

        // Stretch Goal
        // Your password can't be too similar to your other personal information.
        // Your password can't be a commonly used password.
        function checkPasswordStrength() {
            let number = /([0-9])/;
            let alphabets = /([a-zA-Z])/;
            let specialCharacters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;

            if ($('#password').val().length < 8 || !$('#password').val().match(alphabets)) {
                $('#passwordStrengthStatus').removeClass().addClass('weakPassword').html("Weak Your password must contain at least 8 characters and can't be entirely numerical.");
                passwordFullStrength = false;
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(specialCharacters)) {
                    $('#passwordStrengthStatus').removeClass().addClass('strongPassword').html("Strong");
                    passwordFullStrength = true;
                    $('#invalidPassword').hide();
                } else {
                    $('#passwordStrengthStatus').removeClass().addClass('mediumPassword').html("Medium (should include numbers and special characters.)");
                    passwordFullStrength = true;
                }
            }
        }

        // Dropdown search box used for question selection and employee selection
        $("#mySearch").keyup(function () {
            let filter = $("#mySearch").val().toUpperCase();
            let div = document.getElementById("searchDropdown");
            let a = div.getElementsByTagName("a");

            for (let i = 0; i < a.length; i++) {
                let txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        });

        $("#passwordResetForm").on('submit', function (e) {
            if (passwordFullStrength === true) {
                $(this)[0].submit();
            } else {
                e.preventDefault();
            }
        });

        $('#passwordCheck').keyup(function () {
            if ($('#password').val() === $(this).val()) {
                this.setCustomValidity('');
            } else {
                this.setCustomValidity('Passwords must match');
            }
        });

        // Display popovers for element descriptions
        $('[data-toggle="popover"]').popover({trigger: 'hover'});

        $("#profileForm").on('submit', function (e) {
            if (passwordFullStrength === true && $('#password').val() === $('#passwordCheck').val() || $('#password').val().length <= 0 && $('#passwordCheck').val().length <= 0) {
                $('.profileCheck').each(function () {
                    let checkbox_this = $(this);
                    if (checkbox_this.is(":checked") == true) {
                        checkbox_this.attr('value', '1');
                    } else {
                        checkbox_this.prop('checked', true);
                        checkbox_this.attr('value', '0');
                    }
                });
                $(this)[0].submit();
            } else {
                e.preventDefault();
            }
        });

        // Questionnaire Creation
        // Pre Made Questionnaire
        let allQuestions = [];

        // Custom Question
        let customQuestionIndex = 0;

        //Edit questionnaire
        if ($('#questionnaireTitle').length && $('#questionnaireTitle').attr('data-id').length > 0) {
            $.ajax({
                url: '/api/questionnaires/' + $('#questionnaireTitle').attr('data-id') + '/questions/',
                success: function (data) {
                    console.log(data);
                    if (data.questions.length > 0) {
                        for (let i = 0; i < data.questions.length; i++) {
                            if (data.questions[i].is_user_created === true) {
                                let customQuestion = {};
                                customQuestion.custom = 'true';
                                customQuestion.title = data.questions[i].title;
                                customQuestion.response_type = data.questions[i].response_type;
                                let textInner = data.questions[i].title;

                                if (customQuestion.response_type === 'boolean') {
                                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group row col-12"><label class="col-11 checkbox customCheckbox checkbox-checkbox"><input class="questionContent" type="checkbox"/><div class="checkbox_indicator"></div>' + textInner + '</label><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom col-1" aria-label="Close">x</button></div>');
                                } else if (customQuestion.response_type === 'free_text') {
                                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group"><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom" aria-label="Close">x</button><label class="questionContent">' + textInner + '</label><input class="form-control"></div>');
                                } else {
                                    if (data.questions[i].min_label.length > 0) {
                                        customQuestion.min_label = data.questions[i].min_label;
                                    } else {
                                        customQuestion.min_label = 'Min';
                                    }
                                    if (data.questions[i].min_label.length > 0) {
                                        customQuestion.max_label = data.questions[i].max_label;
                                    } else {
                                        customQuestion.max_label = 'Max';
                                    }
                                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group col-12 row"><button type="button" data-id="' + customQuestionIndex + '" class="ml-auto close removeCustom" aria-label="Close">x</button><h4 class="col-12">' + textInner + '</h4><div class="col-4 mr-auto"><h5>' + customQuestion.min_label + '</h5></div><div class="col-4 ml-auto text-right"><h5>' + customQuestion.max_label + '</h5></div><input type="range" min="1" max="5" class="custom-range col-10 mx-auto "></div>');
                                }

                                allQuestions.push(customQuestion);
                                customQuestionIndex = customQuestionIndex + 1;
                            } else {
                                let questionId = data.questions[i].id;
                                let questionType = data.questions[i].response_type;
                                let textInner = data.questions[i].title;
                                let contains = 'False';

                                $.grep(allQuestions, function (e) {
                                    if (e.question === questionId) {
                                        contains = 'True';
                                    }
                                });

                                if (contains !== 'True') {
                                    let generatedQuestion = {};
                                    generatedQuestion.custom = 'false';
                                    generatedQuestion.question = questionId;
                                    generatedQuestion.title = textInner;
                                    allQuestions.push(generatedQuestion);

                                    if (questionType === 'boolean') {
                                        $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group row col-12"><label class="col-11 checkbox customCheckbox checkbox-checkbox"><input class="questionContent" type="checkbox"/><div class="checkbox_indicator"></div>' + textInner + '</label><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom col-1" aria-label="Close">x</button></div>');
                                    } else if (questionType === 'free_text') {
                                        $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group"><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom" aria-label="Close">x</button><label class="questionContent">' + textInner + '</label><input class="form-control"></div>');
                                    } else {
                                        $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group col-12 row"><button type="button" data-id="' + customQuestionIndex + '" class="ml-auto close removeCustom" aria-label="Close">x</button><h4 class="col-12">' + textInner + '</h4><div class="col-4 mr-auto"><h5>' + data.questions[i].min_label + '</h5></div><div class="col-4 ml-auto text-right"><h5>' + data.questions[i].max_label + '</h5></div><input type="range" min="1" max="5" class="custom-range col-10 mx-auto "></div>');
                                    }
                                    customQuestionIndex++;
                                }
                            }
                            $('#questionsValue').val(JSON.stringify(allQuestions));
                        }
                    }
                }, error: function (e) {
                    console.log(e);
                },
                type: 'GET'
            });
        }


        // Questionnaire preview
        $('.questionTemplate').click(function () {
            let questionId = $(this).attr("data-id");
            let questionType = $(this).attr("data-type");
            let textInner = $(this).text();
            let contains = 'False';
            let min_label = 'Min';
            let max_label = 'Max';
            $.grep(allQuestions, function (e) {
                if (typeof e.question !== "undefined") {
                    if (JSON.stringify(e.question) === questionId) {
                        contains = 'True';
                    }
                }
            });

            if (contains === 'True') {
                //   TODO ability to remove from here aswel
            } else {
                let generatedQuestion = {};
                generatedQuestion.custom = 'false';
                generatedQuestion.question = questionId;
                generatedQuestion.title = textInner;
                allQuestions.push(generatedQuestion);

                if (questionType === 'boolean') {
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group row col-12"><label class="col-11 checkbox customCheckbox checkbox-checkbox"><input class="questionContent" type="checkbox"/><div class="checkbox_indicator"></div>' + textInner + '</label><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom col-1" aria-label="Close">x</button></div>');
                } else if (questionType === 'free_text') {
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group"><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom" aria-label="Close">x</button><label class="questionContent">' + textInner + '</label><input class="form-control"></div>');
                } else {
                    if ($(this).attr("data-min").length) {
                        min_label = $(this).attr("data-min")
                    }
                    if ($(this).attr("data-max").length) {
                        max_label = $(this).attr("data-max")
                    }
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group col-12 row"><button type="button" data-id="' + customQuestionIndex + '" class="ml-auto close removeCustom" aria-label="Close">x</button><h4 class="col-12">' + textInner + '</h4><div class="col-4 mr-auto"><h5>' + min_label + '</h5></div><div class="col-4 ml-auto text-right"><h5>' + max_label + '</h5></div><input type="range" min="1" max="5" class="custom-range col-10 mx-auto "></div>');
                }
                customQuestionIndex++;
            }
            $('#questionsValue').val(JSON.stringify(allQuestions));
        });

        $('.questionType').click(function () {
            if ($(this).attr('data-type') === 'scale') {
                $('#rangeSelect').show();
            } else {
                $('#rangeSelect').hide();
                $('#maxLabel').val('');
                $('#minLabel').val('');
            }
        });

        // Custom Question
        $('#addCustom').click(function () {
            if ($('#questionText').val().length > 0) {
                let customQuestion = {};
                customQuestion.custom = 'true';
                customQuestion.title = $('#questionText').val();
                customQuestion.response_type = $('input[name=questionType]:checked', '#customQuestions').val();

                let textInner = $('#questionText').val();

                if ($('input[name=questionType]:checked', '#customQuestions').val() === 'boolean') {
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group row col-12"><label class="col-11 checkbox customCheckbox checkbox-checkbox"><input class="questionContent" type="checkbox"/><div class="checkbox_indicator"></div>' + textInner + '</label><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom col-1" aria-label="Close">x</button></div>');
                } else if ($('input[name=questionType]:checked', '#customQuestions').val() === 'free_text') {
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group"><button type="button" data-id="' + customQuestionIndex + '" class="close removeCustom" aria-label="Close">x</button><label class="questionContent">' + textInner + '</label><input class="form-control"></div>');
                } else {
                    $('#rangeSelect').show();
                    if ($('#minLabel').val().length > 0) {
                        customQuestion.min_label = $('#minLabel').val();
                    } else {
                        customQuestion.min_label = 'Min';
                    }
                    if ($('#maxLabel').val().length > 0) {
                        customQuestion.max_label = $('#maxLabel').val();
                    } else {
                        customQuestion.max_label = 'Max';
                    }
                    $('#maxLabel').val('');
                    $('#minLabel').val('');
                    $('#questionDisplay').append('<div id="close' + customQuestionIndex + '" data-title="' + textInner + '" class="form-group col-12 row"><button type="button" data-id="' + customQuestionIndex + '" class="ml-auto close removeCustom" aria-label="Close">x</button><h4 class="col-12">' + textInner + '</h4><div class="col-4 mr-auto"><h5>' + customQuestion.min_label + '</h5></div><div class="col-4 ml-auto text-right"><h5>' + customQuestion.max_label + '</h5></div><input type="range" min="1" max="5" class="custom-range col-10 mx-auto "></div>');
                }
                allQuestions.push(customQuestion);
                $('#questionText').val('');
                customQuestionIndex = customQuestionIndex + 1;
                $('#questionsValue').val(JSON.stringify(allQuestions));
            }
        });

        $('#questionnaireTitle').keyup(function () {
            $('#theTitle').val($(this).val());
            if ($(this).val().length > 0) {
                $('#questionnaireName').text($(this).val());
            } else {
                $('#questionnaireName').text('Your Questionnaire');
            }
        });

        $('#questionnaireDescription').keyup(function () {
            $('#theDescription').val($(this).val());
            if ($(this).val().length > 0) {
                $('#questionnaireDescriptionText').text($(this).val());
            } else {
                $('#questionnaireDescriptionText').text('Your Questionnaire Description');
            }
        });

        $('#questionDisplay').on('click', '.removeCustom', function () {
            let questionsId = $(this).attr('data-id');
            let questionTitle = JSON.stringify($('#close' + questionsId).attr('data-title'));
            removeQuestion(questionsId, questionTitle);
        });

        // Remove From Array And Template
        function removeQuestion(id, title) {
            $('#close' + id).remove();
            allQuestions = $.grep(allQuestions, function (e) {
                return JSON.stringify(e.title) !== title;
            });
            $('#questionsValue').val(JSON.stringify(allQuestions));
        }

        $("#questionnaireSubmit").click(function (e) {
            e.preventDefault();
            if (selectedMonth && selectedMonth.length) {
                $("#expiry_date").val(selectedMonth);
            }

            if ($("#questionsValue").val().length <= 0) {
                $("#questionsValue").val('null');
            }
            console.log($("#questionsValue").val());
            $('#questionnaireForm').submit();
        });

        // Date pickers
        if ($("#expiryDate").length) {
            if ($("#expiryDate").val().length <= 0) {
                $("#expiryDate").val(moment().add(1, 'months').format("YYYY-MM-DD"));
            }
        }

        let selectedMonth;
        $(".monthPicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',
            maxDate: new Date(),
            setDate: moment(new Date($(this).val())).format("YYYY-MM"),
            onClose: function (dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                selectedMonth = moment(new Date(inst.selectedYear, inst.selectedMonth)).format("YYYY-MM-DD")
            }
        });

        $(".expMonthPicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',
            minDate: new Date(),
            setDate: moment(new Date($(this).val())).format("YYYY-MM"),
            onClose: function (dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                selectedMonth = moment(new Date(inst.selectedYear, inst.selectedMonth)).format("YYYY-MM-DD")
            }
        });

        $(".expMonthPicker").focus(function () {
            $(".ui-datepicker-calendar").hide();
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
        });


        $(".monthPicker").focus(function () {
            $(".ui-datepicker-calendar").hide();
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
        });

        // Report Page (employee index)

        if ($("#reportEndDate").length) {
            if ($("#reportEndDate").val().length) {
                $("#reportEndDate").attr('max', moment().format("YYYY-MM"));
            } else {
                $("#reportEndDate").val(moment().format("YYYY-MM")).attr('max', moment().format("YYYY-MM"));
            }
        }
        // End date needs to be the end

        if ($("#reportStartDate").length) {
            if ($("#reportStartDate").val().length) {
                $("#reportStartDate").attr('max', moment().format("YYYY-MM"));
            } else {
                $("#reportStartDate").val(moment().subtract(1, 'months').format("YYYY-MM")).attr('max', moment().format("YYYY-MM"));
            }
        }

        $("#showAllReportData").click(function () {
            window.location = '/employer?start_date=all&end_date=all';
        });

        $("#reportFilter").click(function () {
            window.location = '/employer?start_date=' + moment($("#reportStartDate").val()).format("YYYY-MM-DD") + '&end_date=' + moment($("#reportEndDate").val()).add(1, 'months').subtract(1, 'days').format("YYYY-MM-DD");
        });

        $("#expiry_date").val(moment().add(1, 'months').format("YYYY-MM-DD"));

        // Print employee report
        $('#print').click(function () {
            var engagementCanvas = document.getElementById("engagementChart");
            var wellbeingChart = document.getElementById("wellbeingChart");
            var newWin = window.open('', 'Print-Window');
            var divToPrint = document.getElementById('reportTable');
            newWin.document.open();
            newWin.document.write("<html><link rel='stylesheet' href='/static/css/bootstrap.min.css\'><link rel='stylesheet' href='/static/css/styles.css\'><body style='margin:0 5%' onload=\"window.print()\"><div style='text-align:center;padding-top:30px'><div style='text-align:center;margin:auto'><h2>Employee Engagement</h2><img style='width:100%' src='" + engagementCanvas.toDataURL() + "'/> </div><div style='text-align:center;margin:auto'><h2>Employee Wellbeing</h2> <img style='width:100%' src='" + wellbeingChart.toDataURL() + "'/></div><div style='margin-top:50px;text-align: center;width: 100%'>" + divToPrint.innerHTML + "</div></div></body></html>");
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        });


        $('#clearAlerts').click(function () {
            $.ajax({
                url: '/api/users/alerts/clear/',
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });

        if ($('#recommendedModal').length && $('#userGoals').length) {
            $.ajax({
                url: 'api/content/video/recommendations/',
                success: function (data) {
                    if (data.length > 0) {
                        for (let i = 0; i < data.length; i++) {
                            $('#recommendedContent').append('<a href="/video/' + data[i].id + '" class="text-center col-md-6 contentLink"><div class="secondaryBackground contentBox"> <div class="contentImgBox"><img alt="' + data[i].title + '" src="' + data[i].image + '" class="contentImg"></div><div class="col-12 row no-overflow marginTop10"><div><svg xmlns="http://www.w3.org/2000/svg" height="20" class="itemIcon" viewBox="0 0 24 24" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon class="icon" points="5 3 19 12 5 21 5 3"></polygon></svg></div><p class="col-10 mx-auto padding5 text-overflow">' + data[i].title + '</p></div></div></a>')
                            if (i > 4) {
                                // Break When on the 6th Result.
                                return false;
                            }
                        }
                    } else {
                        $('#relatedBtn').hide();
                    }
                }, error: function (e) {
                    console.log(e);
                },
                type: 'GET'
            });
        }

        // Check if users first login if so show onboarding page
        if ($('#onboardingModal').length) {
            if ($('#onboardingModal').attr('data-firstLogin') === 'True') {
                $('#onboardingModal').modal('toggle');
                // Set Last Login to now
                $.ajax({
                    url: '/api/users/alerts/clear/',
                    success: function (data) {
                    }, error: function (e) {
                        console.log(e);
                    },
                    type: 'POST'
                });
            }
        }

        let tutorialPgNum = 1;

        hideTutorials();

        function hideTutorials() {
            $(".tutorialPg").each(function () {
                $(this).hide();
            });
            $("#tutorialPg" + tutorialPgNum).show();
        }

        $('.nextTutorialPg').click(function () {
            tutorialPgNum++;
            hideTutorials();
        });

        $('.lastTutorialPg').click(function () {
            tutorialPgNum--;
            hideTutorials();
        });

        $('#onboardingModal').on('hide.bs.modal', function () {
            tutorialPgNum = 1;
            hideTutorials();
        });

        //    Self assessment form
        if ($('#selfAssessmentModal').length) {
            $.ajax({
                url: '/questions/self-assessments',
                success: function (data) {
                    if (data.length > 0) {
                        for (let i = 0; i < data.length; i++) {
                            let minLabel;
                            let maxLabel;
                            console.log("min label", data[i].min_label)
                            if (data[i].min_label) {
                                minLabel = data[i].min_label
                            } else {
                                minLabel = 'Very Low'
                            }
                            if (data[i].min_average_label) {
                                minAvgLabel = data[i].min_average_label
                            } else {
                                minAvgLabel = 'Low'
                            }

                            if (data[i].ok_average_label) {
                                okAvgLabel = data[i].ok_average_label
                            } else {
                                okAvgLabel = 'Ok'
                            }
                            if (data[i].max_average_label) {
                                maxAvgLabel = data[i].max_average_label
                            } else {
                                maxAvgLabel = 'Good'
                            }
                            if (data[i].max_label) {
                                maxLabel = data[i].max_label
                            } else {
                                maxLabel = 'Very Good'
                            }
                            $('#assessmentContent').append('<div class="selfreflectionmodal"> <h4>' + data[i].title + ' </h4> \ <div class="row"> \
                                    <div class="col"><h5>' + minLabel + '</h5></div> \
                                    <div class="col text-center"><h5 class="lowleft">' + minAvgLabel + '</h5></div> \
                                    <div class="col text-center"><h5>' + okAvgLabel + '</h5></div> \
                                    <div class="col text-center"><h5 class="lowright">' + maxAvgLabel + '</h5></div> \
                                    <div class="col text-right"><h5>' + maxLabel + '</h5></div> \
                                    <div class="clearfix d-block w-100"></div> \
                                    <div class="col-md-12 mb-4"><input data-id="' +  data[i].id + '" type="range" min="20" max="100" step="20" class="slider assessmentInput"></div> \
                                    </div></div>');
                            $('[data-toggle="tooltip"]').tooltip();
                        }
                    } else {
                        $('#relatedBtn').hide();
                    }
                }, error: function (e) {
                    console.log(e);
                },
                type: 'GET'
            });
        }

        $('#selfSubmit').click(function () {
            let body = {
                data: [],
                userID: $(this).attr('data-id')
            };
            $(".assessmentInput").each(function () {
                let assessment = {id: $(this).attr('data-id'), value: $(this).val()};
                body.data.push(assessment)
            });
            body = JSON.stringify(body);
            $.ajax({
                url: '/questions/self-assessments',
                data: {
                    body: body
                },
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });

        // Workshop Modal

        // Recommend user video based on the pillar with the lowest self assesment value
        let pillarArray = [];
        if ($('#workshopModal').length) {
            $('.pillarOption').each(function () {
                let pillar = {
                    data: $(this).attr('data-value'),
                    val: $(this).val(),
                    title: $(this).html()
                };
                pillarArray.push(pillar)
            });

            let lowestVal = pillarArray.reduce(function (prev, current) {
                return (prev.data < current.data) ? prev : current
            });

            let selectedPillar = lowestVal.val;
            $('#pillarSelect').val(lowestVal.val);
            $('#selectedPillar').html(lowestVal.title);
            hideworkshopList();


            $('#pillarSelect').change(function () {
                selectedPillar = $(this).val();
                hideworkshopList();
            });

            function hideworkshopList() {
                $('.workshopList').each(function () {
                    $(this).hide()
                });
                $('.' + selectedPillar + 'List').each(function () {
                    $(this).show()
                })
            }
        }

        // Awards submit
        let awards = [];

        $('.employeeSelect').click(function () {

            if ($(this).attr('data-id') !== 'null') {
                $('#employeeName' + $(this).attr('data-award-id')).html($(this).attr('data-employee'));
                let winner = {
                    id: $(this).attr('data-id'),
                    award_id: $(this).attr('data-award-id')
                };
                awards[$(this).attr('data-award-id')] = winner
            } else {
                $('#employeeName' + $(this).attr('data-award-id')).html('Unset');
                awards[$(this).attr('data-award-id')] = null
            }
        });

        $('#awardsSubmit').click(function () {
            let body = [];
            body = JSON.stringify(awards);
            $.ajax({
                url: '/api/awards/',
                data: {
                    winners: body
                },
                success: function (data) {
                    location.reload();
                }, error: function (e) {
                    console.log(e);
                },
                type: 'POST'
            });
        });
        $('#loginForm').submit(function(){
            $('#getUserTimezone').val(Intl.DateTimeFormat().resolvedOptions().timeZone);
        });
    }
);

