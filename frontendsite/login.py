from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django import forms
from django.utils.translation import gettext as _
from rest_framework import status
from django.contrib.auth import views as auth_views
from optimeusers.models import OptimeUser, UserTypeStatus
from optimeusers.serializers import OptimeUserSerializer
from optimeusers.tokens import account_activation_token


def _decode_one_time_link(request, uidb64, token):
    """Auxiliary function decodes url of one-time link"""
    # if another user is logged in, log them out
    if not request.user.is_anonymous:
        logout(request)

    # fetch the user from the invite link
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        return OptimeUser.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, OptimeUser.DoesNotExist):
        return None


class RegistrationView(View):
    """
       get:
       serves the registration form, validating the invite link attached and serving 404 if it's invalid

       post:
       submits the registration form, updating the user object. Redirects to the introductory questionnaire if the
       company has one configured or to the index page if not
    """

    def get(self, request, uidb64, token):
        user = _decode_one_time_link(request, uidb64, token)

        # validate the token
        if user is not None and account_activation_token.check_token(user, token):
            login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')

            return render(request, template_name='frontendsite/registration.html')

        else:
            # invalid link
            return render(request, '404.html', status=status.HTTP_404_NOT_FOUND)

    def post(self, request, uidb64, token):
        password = request.POST.get('password')

        user = request.user

        # Set password...
        user.set_password(password)
        user.email_confirmed = True

        user.save()

        # changing password will log the user out.. log them back in
        login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')

        serializer = OptimeUserSerializer(user, data=request.POST)

        if serializer.is_valid():
            serializer.save()

            # finished! Redirect the user to the introductory questionnaire (if exists)
            if user.company.introductory_questionnaire is not None:
                return redirect('frontendsite-questionnaires-detail', user.company.introductory_questionnaire.pk)

            # if it doesn't exist redirect to index
            return redirect('frontendsite-index')

        return render(request, template_name='frontendsite/registration.html',
                      context={'error': serializer.errors},
                      status=status.HTTP_400_BAD_REQUEST)


class PasswordResetForm(auth_views.PasswordResetForm):
    email = forms.EmailField(label=_("Email"),
                             widget=forms.TextInput(attrs={'placeholder': 'Email'}),
                             max_length=254)


class PasswordResetViewInherit(auth_views.PasswordResetView):
    form_class = PasswordResetForm

class UserForgotPasswordView(View):
    """
       get:
       serves the forgot password form, validating the invite link attached and serving 404 if it's invalid

       post:
       submits the forgot password form, updating the user object. Redirects to the index page
    """

    def get(self, request, uidb64, token):
        user = _decode_one_time_link(request, uidb64, token)

        # validate the token
        if user is not None and account_activation_token.check_token(user, token):
            login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')

            return render(request, template_name='frontendsite/forgot_password.html')

        else:
            # invalid link
            return render(request, '404.html', status=status.HTTP_404_NOT_FOUND)

    def post(self, request, uidb64, token):
        password = request.POST.get('password')

        user = request.user

        # Set password...
        user.set_password(password)
        user.save()

        # changing password will log the user out.. log them back in
        login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')

        # finished! Redirect the user to the index page
        return redirect('frontendsite-index')


class LoginView(View):
    """
       get:
       serves the login form

       post:
       submits the login form and redirects to the appropriate index page for the user type
    """

    def get(self, request):
        # render login page
        return render(request, template_name='frontendsite/login.html')

    def post(self, request):
        # fetch creds
        print("login>>>>>>>>>>>>>>>>.")
        email = request.POST.get('email')
        password = request.POST.get('password')

        if email is None or len(email) < 1:
            return render(request, template_name='frontendsite/login.html',
                          context={'error': {'email': ['Please provide an email address!']}},
                          status=status.HTTP_400_BAD_REQUEST)
        if password is None or len(password) < 1:
            return render(request, template_name='frontendsite/login.html',
                          context={'error': {'password': ['Please provide a password!']}},
                          status=status.HTTP_400_BAD_REQUEST)

        # validate email
        try:
            validate_email(email)
        except ValidationError:
            return render(request, template_name='frontendsite/login.html',
                          context={'error': {'email': ['Invalid email/password!']}},
                          status=status.HTTP_403_FORBIDDEN)

        try:
            user = OptimeUser.objects.get(email__iexact=email)
        except OptimeUser.DoesNotExist:
            return render(request, template_name='frontendsite/login.html',
                          context={'error': {'email': ['Invalid email/password!']}},
                          status=status.HTTP_403_FORBIDDEN)

        # Check password...
        auth_user = authenticate(username=user.username, password=password, request=request)
        if auth_user is not None:
            # A backend authenticated the credentials
            # Success
            request.session['user_timezone'] = request.POST.get('timeZone')
            pass
        else:
            # No backend authenticated the credentials
            return render(request, template_name='frontendsite/login.html',
                          context={'error': {'password': ['Invalid email/password!']}},
                          status=status.HTTP_403_FORBIDDEN)

        # store the previous value of last_login. This is used for simulating alerts
        user.last_last_login = user.last_login
        user.save()
        login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')

        # if I'm an employer, render me the employer index
        if user.user_type == UserTypeStatus.Employer:
            return redirect('frontendsite-employer-index')

        # if I'm an admin, redirect me to the admin panel
        if user.is_superuser:
            return redirect('admin:index')

        # otherwise render me the employee index
        return redirect('frontendsite-index')


class LogoutView(View):
    """
        get:
        logs out and redirects to the login page
    """

    def get(self, request):
        logout(request)
        return redirect('frontendsite-login')
