import collections
from datetime import datetime, date, timedelta
from json import JSONDecodeError

from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
from rest_framework import status
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView
from content.views import get_published_content_by_pillar, \
    get_published_content_by_content_type, get_published_content

from optime.settings import PAGINATION_RESULTS_PER_PAGE
from questionnaires.models import Questionnaire, QuestionnaireResponse, Question, QuestionTypeStatus, \
    QuestionnaireQuestion, \
    BooleanQuestionnaireQuestionResponse, FreeTextQuestionnaireQuestionResponse, ScaleQuestionnaireQuestionResponse, \
    SelfAssessmentQuestionResponse
from questionnaires.permissions import EmployerPermission, EmployeePermission
from questionnaires.serializers import QuestionnaireSerializer, QuestionSerializer, \
    BooleanQuestionnaireQuestionResponseSerializer, FreeTextQuestionnaireQuestionResponseSerializer, \
    ScaleQuestionnaireQuestionResponseSerializer
from questionnaires.views import get_question_options
from .views import render_404
from .views import get_user_and_greeting



class QuestionnaireView(APIView):
    """
        Employer access to questionnaires

        get:
        serves paginated questionnaires, separated into new_questionnaires (have not expired) and old_questionnaires
        (have expired). Introductory questionnaires are excluded because this can be edited via settings
    """

    permission_classes = (EmployerPermission,)

    def get(self, request):
        # get new & old questionnaires relating to this company
        questionnaires = Questionnaire.objects.filter(company=request.user.company)
        intro_questionnaires = (q.pk for q in questionnaires.all() if q.is_introductory_questionnaire)
        new_questionnaires = questionnaires.filter(expiry_date__gt=datetime.now()) \
            .exclude(pk__in=intro_questionnaires).order_by('-created_at')
        old_questionnaires = questionnaires.filter(expiry_date__lt=datetime.now()) \
            .exclude(pk__in=intro_questionnaires).order_by('-created_at')

        # paginate old questionnaires
        # requested page
        page = request.query_params.get('page')

        # default to page 1
        if page is None:
            page = 1

        # apply pagination to results
        paginator = Paginator(old_questionnaires, PAGINATION_RESULTS_PER_PAGE)
        old_questionnaires = paginator.get_page(page)

        # pagination data for the context
        current_page = old_questionnaires.number
        num_pages = old_questionnaires.paginator.num_pages

        #get user first name and greeting
        user_data = get_user_and_greeting(request)

        return render(request, template_name='employer_dashboard/questionnaires.html',
                      context={'old_questionnaires': old_questionnaires, 'new_questionnaires': new_questionnaires,
                               'current_page': current_page, 'num_pages': num_pages, 'pages': range(num_pages + 1),
                               'intro_questionnaire':get_questionnaire_responses(request,None,introductory_questionnaire=True),
                               'current_user': user_data.get('current_user'),
                               'greet': user_data.get('greet')
                               })


class EmployeeQuestionnaireView(APIView):
    """
        Employee access to questionnaires

        get:
        serves paginated questionnaires, separated into new_questionnaires (published questionnaires the user hasn't
        completed) and old_questionnaires (published questionnaires the user has completed). Questionnaires which haven't
        been published, and those which have expired without the user completing them are not displayed
    """

    def get(self, request):
        # get questionnaires relating to this company
        questionnaires = Questionnaire.objects.filter(company=request.user.company, is_published=True)

        # get the list of questionnaires the employee has responded to
        responses = QuestionnaireResponse.objects.filter(user=request.user).values_list('questionnaire_id', flat=True)

        # current questionnaires are those which the employee has not completed, and they have not expired
        current_questionnaires = questionnaires.filter(expiry_date__gt=datetime.now()).exclude(id__in=responses) \
            .order_by('-created_at')

        # past surveys are questionnaires which the employee has completed
        past_questionnaires = QuestionnaireResponse.objects.filter(user=request.user).order_by('-created_at')

        # paginate past surveys
        # requested page
        page = request.query_params.get('page')

        # default to page 1
        if page is None:
            page = 1

        # apply pagination to results
        paginator = Paginator(past_questionnaires, PAGINATION_RESULTS_PER_PAGE)
        past_questionnaires = paginator.get_page(page)

        # pagination data for the context
        current_page = past_questionnaires.number
        num_pages = past_questionnaires.paginator.num_pages
        
        #get user first name and greeting
        user_data = get_user_and_greeting(request)

        get_all_videos = get_published_content(request.user)
        # filter for videos 
        form_content_type = request.GET.get("type", 'all')
        form_pillar = request.GET.get("pillar", 'all')
        # render index page

        if request.GET.get('all_data') == 'true':
            form_all_data = True
        else:
            form_all_data = False

        if form_content_type == 'all' and form_pillar != 'all':
            get_all_videos = get_published_content_by_pillar(request.user, form_pillar)
        elif form_content_type != 'all' and form_pillar == 'all':
            get_all_videos = get_published_content_by_content_type(request.user, form_content_type)
        elif form_content_type == 'all' and form_pillar == 'all':
            get_all_videos = get_published_content(request.user)
        else:
            types = get_published_content_by_content_type(request.user, form_content_type)
            pillar = get_published_content_by_pillar(request.user, form_pillar)

            if types is None:
                types = []

            if pillar is None:
                pillar = None

            get_all_videos = list(set(types) & set(pillar))


        return render(request, template_name='frontendsite/surveys.html',
                      context={'new_questionnaires': current_questionnaires, 'old_questionnaires': past_questionnaires,
                                'current_page': current_page, 'num_pages': num_pages,
                                'current_user': user_data.get('current_user'),
                                'greet': user_data.get('greet'),'all_content': get_all_videos,'current_content_type': form_content_type,
                               'current_pillar': form_pillar,
                                'pages': range(num_pages + 1)})

class EmployeeQuestionnaireDetailView(APIView):
    """
        Employee access to a questionnaire in detail

        get:
        serves template to view the questionnaire

        post:
        submits a response to the questionnaire, and redirects to the questionnaires list
    """

    permission_classes = (EmployeePermission,)

    def get_object(self, request, pk):
        try:
            questionnaire = Questionnaire.objects.get(pk=pk)
            self.check_object_permissions(request, questionnaire)
            return questionnaire
        except Questionnaire.DoesNotExist:
            return render_404(request)

    def get(self, request, pk):
        questionnaire = self.get_object(request, pk)

        return render(request, template_name='frontendsite/survey.html', context={'questionnaire': questionnaire})

    # view POSTs a response to a questionnaire
    def post(self, request, pk):
        questionnaire = self.get_object(request, pk)

        # fetch question response param
        try:
            questions = json.loads(request.POST.get('questions'))
        except (ValueError, JSONDecodeError):
            return render(request, template_name='frontendsite/survey.html',
                          context={'questionnaire': questionnaire, 'error': {'questions': ['Invalid request']}},
                          status=status.HTTP_400_BAD_REQUEST)

        response = QuestionnaireResponse(user=request.user, questionnaire=questionnaire, created_at=datetime.now())
        response.save()

        for question in questions:
            # get the question this is in response to
            try:
                questionnaire_questions = QuestionnaireQuestion.objects.filter(questionnaire=questionnaire)
                print(">>>>>>>>>>>>>>>>", question)
                questionnaire_question = questionnaire_questions.get(question_id=question['id'])

            except QuestionnaireQuestion.DoesNotExist:
                response.delete()

                return render(request, template_name='frontendsite/survey.html',
                              context={'questionnaire': questionnaire,
                                       'error': {'questions': ['Invalid question passed']}},
                              status=status.HTTP_400_BAD_REQUEST)

            if question['response_type'] == QuestionTypeStatus.Boolean:
                q = BooleanQuestionnaireQuestionResponse(questionnaire_response=response,
                                                         questionnaire_question=questionnaire_question)
                serializer = BooleanQuestionnaireQuestionResponseSerializer(q, data=question)

                if serializer.is_valid():
                    serializer.save()
                else:
                    response.delete()

                    return render(request, template_name='frontendsite/survey.html',
                                  context={'questionnaire': questionnaire, 'error': serializer.errors},
                                  status=status.HTTP_400_BAD_REQUEST)

            elif question['response_type'] == QuestionTypeStatus.FreeText:
                q = FreeTextQuestionnaireQuestionResponse(questionnaire_response=response,
                                                          questionnaire_question=questionnaire_question)
                serializer = FreeTextQuestionnaireQuestionResponseSerializer(q, data=question)

                if serializer.is_valid():
                    serializer.save()
                else:
                    response.delete()

                    return render(request, template_name='frontendsite/survey.html',
                                  context={'questionnaire': questionnaire, 'error': serializer.errors},
                                  status=status.HTTP_400_BAD_REQUEST)

            elif question['response_type'] == QuestionTypeStatus.Scale:
                q = ScaleQuestionnaireQuestionResponse(questionnaire_response=response,
                                                       questionnaire_question=questionnaire_question)
                serializer = ScaleQuestionnaireQuestionResponseSerializer(q, data=question)

                if serializer.is_valid():
                    serializer.save()
                else:
                    response.delete()

                    return render(request, template_name='frontendsite/survey.html',
                                  context={'questionnaire': questionnaire, 'error': serializer.errors},
                                  status=status.HTTP_400_BAD_REQUEST)

            else:
                response.delete()

                return render(request, template_name='frontendsite/survey.html',
                              context={'questionnaire': questionnaire,
                                       'error': {'question': ['Unrecognised question response_type']}},
                              status=status.HTTP_400_BAD_REQUEST)

        # success! redirect to questionnaires list view
        return redirect('frontendsite-questionnaires')


class CreateQuestionnaireView(APIView):
    """
        Employer access for creating a new questionnaire

        get:
        serves the form for creating a new questionnaire

        post:
        creates the questionnaire and redirects to the questionnaires list view
    """
    permission_classes = (EmployerPermission,)

    def get(self, request):
        is_introductory = request.query_params.get('is_introductory')

        if is_introductory == 'true':
            is_introductory = True

        return render(request, template_name='employer_dashboard/create_questionnaire.html',
                      context={'question_options': get_question_options(), 'is_introductory': is_introductory})

    def post(self, request):
        is_introductory = request.POST.get('is_introductory')

        if is_introductory == 'true':
            is_introductory = True

        questionnaire = Questionnaire(company=request.user.company, created_at=datetime.now())

        data = request.POST.copy()

        # if this is the introductory questionnaire give it a meaningless expiry_date
        if is_introductory:
            data.__setitem__('expiry_date', (date.today() + timedelta(days=36500)))

        # validate the basics
        serializer = QuestionnaireSerializer(questionnaire, data=data)

        if serializer.is_valid():
            questions_param = request.POST.get('questions')
            questions = None

            # are there questions to parse?
            if questions_param is not None and questions_param != 'null':
                try:
                    questions = json.loads(questions_param)
                except (ValueError, JSONDecodeError):
                    return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                  context={'question_options': get_question_options(),
                                           'error': {'questions': ['Invalid questions']},
                                           'is_introductory': is_introductory},
                                  status=status.HTTP_400_BAD_REQUEST)

            # save the basics
            questionnaire = serializer.save()

            if questions is not None and len(questions) > 0:
                # add the questions selected
                for i, q in enumerate(questions):
                    # does this question already exist?
                    if q.get('custom') is False or q.get('custom') == 'false':
                        try:
                            # fetch the pre-made question they have selected
                            question = Question.objects.get(pk=q.get('question'))

                        except Question.DoesNotExist:
                            questionnaire.delete()

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'error': {'question': ['Could not find question with this id']},
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_404_NOT_FOUND)

                        except ValueError:
                            questionnaire.delete()

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'error': {'error': ['Something went wrong']},
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_400_BAD_REQUEST)

                    # create custom question
                    else:
                        question = Question(is_user_created=True)
                        question_serializer = QuestionSerializer(question, data=q)

                        if question_serializer.is_valid():
                            question = question_serializer.save()
                        else:
                            questionnaire.delete()

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'error': question_serializer.errors,
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_400_BAD_REQUEST)

                    questionnaire.add_question(question, order=i)

            if is_introductory is True:
                # update company reference
                request.user.company.introductory_questionnaire = questionnaire
                request.user.company.save()

                # redirect to questionnaires list with success message
                return redirect('frontendsite-employer-profile')

            return redirect('frontendsite-employer-questionnaires')

        return render(request, template_name='employer_dashboard/create_questionnaire.html',
                      context={'question_options': get_question_options(),
                               'error': serializer.errors,
                               'is_introductory': is_introductory},
                      status=status.HTTP_400_BAD_REQUEST)


class QuestionnaireDetailView(APIView):
    """
        Employer access for viewing an existing questionnaire

        get:
        serves the create_questionnaire template, with an existing questionnaire in the context

        post:
        submitting a form for editing the questionnaire
    """

    permission_classes = (EmployerPermission,)

    def get_object(self, request, pk):
        try:
            questionnaire = Questionnaire.objects.get(pk=pk)
            self.check_object_permissions(request, questionnaire)
            return questionnaire
        except Questionnaire.DoesNotExist:
            return render_404(request)

    def get(self, request, pk):
        questionnaire = self.get_object(request, pk)

        is_introductory = request.query_params.get('is_introductory')

        if is_introductory == 'true':
            is_introductory = True

        return render(request, template_name='employer_dashboard/create_questionnaire.html',
                      context={'question_options': get_question_options(), 'questionnaire': questionnaire,
                               'is_introductory': is_introductory})

    def post(self, request, pk):
        questionnaire = self.get_object(request, pk)

        is_introductory = request.POST.get('is_introductory')

        if is_introductory == 'true':
            is_introductory = True

        serializer = QuestionnaireSerializer(questionnaire, data=request.POST, partial=True)

        if serializer.is_valid():
            # make a backup of questions (to reset state if we encounter an error)
            prior_questions = questionnaire.questions

            # clear prior questions
            questionnaire.clear_questions()

            # are there new questions to parse?
            questions_param = request.POST.get('questions')
            questions = None

            if questions_param is not None and questions_param != 'null':
                try:
                    questions = json.loads(questions_param)
                except (ValueError, JSONDecodeError):
                    questionnaire.clear_questions()
                    questionnaire.set_questions(prior_questions)

                    return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                  context={'question_options': get_question_options(), 'questionnaire': questionnaire,
                                           'error': {'questions': ['Invalid questions']},
                                           'is_introductory': is_introductory},
                                  status=status.HTTP_400_BAD_REQUEST)

            if questions is not None and len(questions) > 0:
                # add the questions selected
                for i, q in enumerate(questions):
                    # does this question already exist?
                    if q.get('custom') is False or q.get('custom') == 'false':
                        try:
                            # fetch the pre-made question they have selected
                            question = Question.objects.get(pk=q.get('question'))

                        except Question.DoesNotExist:
                            questionnaire.clear_questions()
                            questionnaire.set_questions(prior_questions)

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'questionnaire': questionnaire,
                                                   'error': {'question': ['Could not find question with this id']},
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_404_NOT_FOUND)

                        except ValueError:
                            questionnaire.clear_questions()
                            questionnaire.set_questions(prior_questions)

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'questionnaire': questionnaire,
                                                   'error': {'error': ['Something went wrong']},
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_400_BAD_REQUEST)

                    # create custom question
                    else:
                        question = Question(is_user_created=True)
                        question_serializer = QuestionSerializer(question, data=q)

                        if question_serializer.is_valid():
                            question = question_serializer.save()
                        else:
                            questionnaire.clear_questions()
                            questionnaire.set_questions(prior_questions)

                            return render(request, template_name='employer_dashboard/create_questionnaire.html',
                                          context={'question_options': get_question_options(),
                                                   'questionnaire': questionnaire,
                                                   'error': question_serializer.errors,
                                                   'is_introductory': is_introductory},
                                          status=status.HTTP_400_BAD_REQUEST)

                    questionnaire.add_question(question, order=i)

            serializer.save()

            # introductory questionnaires redirect to a different page
            if is_introductory is True:
                return redirect('frontendsite-employer-profile')

            return redirect('frontendsite-employer-questionnaires')

        return render(request, template_name='employer_dashboard/create_questionnaire.html',
                      context={'question_options': get_question_options(), 'questionnaire': questionnaire,
                               'error': serializer.errors, 'is_introductory': is_introductory},
                      status=status.HTTP_400_BAD_REQUEST)


def populate_base_results(answers, question):
    """Returns base results before results are added"""
    base_results = collections.OrderedDict()
    if question.response_type == QuestionTypeStatus.Boolean:
        base_results[True] = 0
        base_results[False] = 0

    if question.response_type == QuestionTypeStatus.Scale:
        for i in range(1, 6):
            if i not in answers:
                base_results[i] = 0
    return base_results


def calculate_results(num_of_answers, answers):
    """Returns answer percentages"""
    # avoid division by 0
    if num_of_answers == 0:
        return 0
    # get count of responses to questionnaire
    for key, value in answers.items():
        answers[key] = (value / num_of_answers) * 100

    # return percentage of employees with responses
    return answers


def get_question_response_type(response_type):
    """Returns appropriate QuestionnaireQuestionResponse"""
    try:
        response_types = {QuestionTypeStatus.FreeText: FreeTextQuestionnaireQuestionResponse,
                          QuestionTypeStatus.Boolean: BooleanQuestionnaireQuestionResponse,
                          QuestionTypeStatus.Scale: ScaleQuestionnaireQuestionResponse}
    except KeyError as e:
        print('KeyError "%s"' % str(e))

    return response_types[response_type]


def count_answers(answers, question):
    """counts answers to be used for getting the answer percentage"""

    # populate base results before adding counts for particular answers
    count = populate_base_results(answers, question)

    for answer in answers:
        if answer.response in count:
            count[answer.response] += 1
        else:
            count[answer.response] = 1
    return count


def _404_response():
    """Returns a response with a 404 error"""
    return Response(data={'error': {'questionnaire': ['Could not find questionnaire with this ID!']}},
                    status=status.HTTP_404_NOT_FOUND)

def get_questionnaire_responses(request,pk, introductory_questionnaire=False):
    """
    get all questionnaire responses
    checks for introductory_questionnaire and if not use the primary key
    """
    try:

        if introductory_questionnaire:
            questionnaire = request.user.company.introductory_questionnaire
        elif pk is not None:
            questionnaire = Questionnaire.objects.get(pk=pk)
        questions = QuestionnaireQuestion.objects.all().filter(questionnaire=questionnaire)

        question_answers = {}
        for question in questions:
            answers = get_question_response_type(question.question.response_type).objects.filter(
                questionnaire_question=question)
            if question.question.response_type != QuestionTypeStatus.FreeText:
                num_of_answers = answers.count()
                count = count_answers(answers, question.question)
                question_answers[question.question] = calculate_results(num_of_answers, count)
            if question.question.response_type == QuestionTypeStatus.FreeText:
                question_answers[question.question] = answers

    except Questionnaire.DoesNotExist or QuestionnaireQuestion.DoesNotExist:
        return _404_response()
    return questionnaire, question_answers


class QuestionnaireResponsesView(View):
    """
        get
        returns all Questionnaire responses
    """
    permission_classes = (EmployerPermission,)

    def get(self, request, pk):
        questionnaire, question_answers = get_questionnaire_responses(request,pk)

        return render(request, template_name='employer_dashboard/responses.html',
                      context={'questionnaire': questionnaire, 'responses': question_answers})


class IntroQuestionnaireResponsesView(View):
    """
        get
        returns introductory Questionnaire responses
    """
    permission_classes = (EmployerPermission,)

    def get(self, request):
        questionnaire, question_answers = get_questionnaire_responses(request,None,introductory_questionnaire=True)

        return render(request, template_name='employer_dashboard/responses.html',
                          context={'questionnaire': questionnaire, 'responses': question_answers})

class QuestionnaireResponsesTextView(View):
    """
        get
        returns free_text responses
    """
    permission_classes = (EmployerPermission,)

    def get(self, request, questionnaire_pk, response_pk):
        try:
            questionnaire = Questionnaire.objects.get(pk=questionnaire_pk)
            question = QuestionnaireQuestion.objects.all().filter(question=response_pk,
                                                                  questionnaire=questionnaire_pk).first()
            answers = get_question_response_type(question.question.response_type).objects.filter(
                questionnaire_question=question)

            paginator = Paginator(answers, PAGINATION_RESULTS_PER_PAGE)
            page = request.GET.get('page', 1)
            final_answers = paginator.get_page(page)
            current_page = final_answers.number
            num_pages = final_answers.paginator.num_pages

        except Questionnaire.DoesNotExist or QuestionnaireQuestion.DoesNotExist:
            return _404_response()
        return render(request, template_name='employer_dashboard/response.html',
                      context={'questionnaire': questionnaire, 'question': question.question, 'answers': final_answers,
                               'current_page': current_page, 'num_pages': num_pages, 'pages': range(num_pages + 1)})


class QuestionsSelfAssessmentDetailView(APIView):
    """
        get
        returns questions based on whether they are self assessment and scale
    """

    def get(self, request):
        questions = Question.objects.filter(self_assessment=True, show_in_self_reflection_modal=True,
                                            response_type='scale')

        return Response(list(questions.values()))

    """
        post
        saves SelfAssessmentQuestionResponse
    """

    def post(self, request):
        questions = json.loads(request.POST['body'])
        for question in questions['data']:
            item = SelfAssessmentQuestionResponse(question=Question.objects.get(id=int(question['id'])),
                                                  response=int(question['value']), user=request.user)
            item.save()
        return HttpResponse(status=204)
