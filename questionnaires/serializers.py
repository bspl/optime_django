from django.core.validators import MinValueValidator, MaxValueValidator
from rest_framework import serializers

from companies.serializers import CompanySerializer
from content.models import PillarTypeStatus
from optime.settings import DATETIME_FORMAT, DATETIME_INPUT_FORMATS
from optimeusers.serializers import OptimeUserSerializer
from .models import Question, Questionnaire, QuestionTypeStatus, QuestionnaireResponse, QuestionnaireQuestion, \
    BooleanQuestionnaireQuestionResponse, FreeTextQuestionnaireQuestionResponse, ScaleQuestionnaireQuestionResponse


class QuestionnaireSerializer(serializers.Serializer):
    company = CompanySerializer(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    is_published = serializers.BooleanField(required=False)
    title = serializers.CharField(required=False, allow_blank=True, max_length=255)
    description = serializers.CharField(required=False, allow_null=False, allow_blank=True)
    expiry_date = serializers.DateField(required=True, format=DATETIME_FORMAT, input_formats=DATETIME_INPUT_FORMATS)

    def create(self, validated_data):
        return Questionnaire.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.is_published = validated_data.get('is_published', instance.is_published)
        instance.expiry_date = validated_data.get('expiry_date', instance.expiry_date)

        instance.save()

        return instance


class QuestionSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    title = serializers.CharField(required=True, allow_blank=False, max_length=255)
    response_type = serializers.ChoiceField(read_only=False, choices=QuestionTypeStatus.choices(), allow_blank=False)
    is_user_created = serializers.BooleanField(required=False)
    min_label = serializers.CharField(required=False, allow_blank=True, max_length=32)
    min_average_label = serializers.CharField(required=False, allow_blank=True, max_length=32)
    ok_average_label = serializers.CharField(required=False, allow_blank=True, max_length=32)
    max_average_label = serializers.CharField(required=False, allow_blank=True, max_length=32)
    max_label = serializers.CharField(required=False, allow_blank=True, max_length=32)
    pillar = serializers.ChoiceField(required=False, choices=PillarTypeStatus.choices(), allow_blank=False)

    def create(self, validated_data):
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.response_type = validated_data.get('response_type', instance.response_type)
        instance.is_user_created = validated_data.get('is_user_created', instance.is_user_created)
        instance.min_label = validated_data.get('min_label', instance.min_label)
        instance.min_average_label = validated_data.get('min_average_label', instance.min_average_label)
        instance.ok_average_label = validated_data.get('ok_average_label', instance.ok_average_label)
        instance.max_average_label = validated_data.get('max_average_label', instance.max_average_label)
        instance.max_label = validated_data.get('max_label', instance.max_label)
        instance.pillar = validated_data.get('pillar', instance.pillar)

        instance.save()

        return instance


class QuestionnaireResponseSerializer(serializers.Serializer):
    user = OptimeUserSerializer(read_only=True)
    questionnaire = QuestionnaireSerializer(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)

    def create(self, validated_data):
        return QuestionnaireResponse.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.save()

        return instance


class QuestionnaireQuestionSerializer(serializers.Serializer):
    questionnaire = QuestionnaireSerializer(read_only=True)
    question = QuestionSerializer(read_only=True)
    order = serializers.IntegerField(required=True, validators=[MinValueValidator(0)])

    def create(self, validated_data):
        return QuestionnaireQuestion.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.order = validated_data.get('order', instance.order)
        instance.save()

        return instance


class BooleanQuestionnaireQuestionResponseSerializer(serializers.Serializer):
    questionnaire_response = QuestionnaireResponseSerializer(read_only=True)
    questionnaire_question = QuestionnaireQuestionSerializer(read_only=True)
    response = serializers.BooleanField(required=False, allow_null=False)

    def create(self, validated_data):
        return BooleanQuestionnaireQuestionResponse.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.response = validated_data.get('response', instance.response)

        instance.save()

        return instance


class FreeTextQuestionnaireQuestionResponseSerializer(serializers.Serializer):
    questionnaire_response = QuestionnaireResponseSerializer(read_only=True)
    questionnaire_question = QuestionnaireQuestionSerializer(read_only=True)
    response = serializers.CharField(required=False, allow_null=False, allow_blank=True)

    def create(self, validated_data):
        return FreeTextQuestionnaireQuestionResponse.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.response = validated_data.get('response', instance.response)

        instance.save()

        return instance


class ScaleQuestionnaireQuestionResponseSerializer(serializers.Serializer):
    questionnaire_response = QuestionnaireResponseSerializer(read_only=True)
    questionnaire_question = QuestionnaireQuestionSerializer(read_only=True)
    response = serializers.IntegerField(required=False, allow_null=False, validators=[MinValueValidator(20),
                                                                                      MaxValueValidator(100)])

    def create(self, validated_data):
        return ScaleQuestionnaireQuestionResponse.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.response = validated_data.get('response', instance.response)

        instance.save()

        return instance
