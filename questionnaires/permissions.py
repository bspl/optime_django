from rest_framework.permissions import BasePermission

from optimeusers.models import UserTypeStatus


def is_employer(user):
    """Returns True if the user is of employer type"""
    if user.company is not None and user.user_type == UserTypeStatus.Employer:
        return True
    return False


# auxiliary function tests if user and object share a company
def is_in_company(user, obj):
    """Returns True if the user and object share the same company"""
    if obj.company is None:
        return False

    # object must be part of this company
    return user.company.id == obj.company.id


class EmployerPermission(BasePermission):
    """
        Permissions for cases where the user must be an employer

        has_permission
        granted if the user is an employer

        has_object_permission
        granted if the user is an employer of the company of the object they are trying to access
    """

    def has_permission(self, request, view):
        return is_employer(request.user)

    def has_object_permission(self, request, view, obj):
        if not is_employer(request.user):
            return False

        return is_in_company(request.user, obj)


class EmployeePermission(BasePermission):
    """
        Permissions for cases where the user must be an employee (or greater level of permission)

        has_permission
        granted if the user is authenticated

        has_object_permission
        granted if the user is in the same company as the object they are trying to access
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return is_in_company(request.user, obj)
