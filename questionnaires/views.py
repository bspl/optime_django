from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Question, Questionnaire, QuestionnaireQuestion
from .permissions import EmployerPermission
from .serializers import QuestionSerializer


def get_question_options():
    """Returns Questions not created by a user (created via the admin panel), and thus not specific to one company"""
    return Question.objects.filter(is_user_created=False)

def _404_response():
    """Returns a response with a 404 error"""
    return Response(data={'error': {'questionnaire': ['Could not find questionnaire with this ID!']}},
                    status=status.HTTP_404_NOT_FOUND)


# view to publish a questionnaire
class PublishQuestionnaireView(APIView):
    """
        post
        Publishes a questionnaire by setting is_published to True and saving the object (save() handles the rest)
    """

    permission_classes = (EmployerPermission,)

    def post(self, request):
        # fetch the id parameter
        id = request.POST.get('id')

        if id is None:
            return Response(data={'error': {'questionnaire': ['please provide a questionnaire ID']}},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            # fetch the questionnaire requested to publish
            questionnaire = Questionnaire.objects.get(id=id)

            # check that requesting user is employer of this company
            self.check_object_permissions(request, questionnaire)

            # update the questionnaire's status
            questionnaire.is_published = True
            questionnaire.save()

            return Response(status=status.HTTP_204_NO_CONTENT)

        except Questionnaire.DoesNotExist:
            return _404_response()


# view to delete a questionnaire
class QuestionnaireDetailView(APIView):
    """
        delete
        deletes the parameterised questionnaire object (if the user has permission)
    """

    permission_classes = (EmployerPermission,)

    def get_object(self, request, pk):
        try:
            questionnaire = Questionnaire.objects.get(pk=pk)
            self.check_object_permissions(request, questionnaire)
            return questionnaire
        except Questionnaire.DoesNotExist:
            return _404_response()

    def delete(self, request, pk):
        questionnaire = self.get_object(request, pk)

        questionnaire.delete()

        return Response(data={'success': 'Deleted meeting'}, status=status.HTTP_200_OK)


class QuestionnaireQuestionView(APIView):
    """
        get
        returns all Question objects in a parameterised Questionnaire, in an array
    """
    permission_classes = (EmployerPermission,)

    def get_object(self, request, pk):
        try:
            questionnaire = Questionnaire.objects.get(pk=pk)
            self.check_object_permissions(request, questionnaire)
            return questionnaire
        except Questionnaire.DoesNotExist:
            return _404_response()

    # view to GET all questions in a questionnaire
    def get(self, request, pk):
        questionnaire = self.get_object(request, pk)

        questions = QuestionSerializer(questionnaire.questions, many=True)

        return Response(data={'questions': questions.data}, status=status.HTTP_200_OK)


class QuestionnaireQuestionDetailView(APIView):
    """
        delete
        removes a question from a questionnaire (if the user has permission)
    """

    def get_object(self, request, pk):
        try:
            question = QuestionnaireQuestion.objects.get(pk=pk)
            self.check_object_permissions(request, question)
            return question
        except QuestionnaireQuestion.DoesNotExist:
            return _404_response()

    # view to remove question from questionnaire
    def delete(self, request, pk):
        question = self.get_object(request, pk)

        question.delete()

        return Response(data={'success': 'Deleted meeting'}, status=status.HTTP_200_OK)
