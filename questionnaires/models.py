from datetime import datetime, timedelta

from celery.utils.log import get_task_logger
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.template.loader import render_to_string

from companies.models import Company
from content.models import PillarTypeStatus, ContentType
from optime.celery import app
from optime.settings import FROM_EMAIL, SECONDS_WELLBEING_QUESTIONNAIRE_CYCLE
from optimeusers.models import OptimeUser

logger = get_task_logger(__name__)


class QuestionTypeStatus(object):
    """Defines the response type of the question, and thus the subclass of the QuestionnaireQuestionResponse"""
    Boolean = 'boolean'
    FreeText = 'free_text'
    Scale = 'scale'

    @classmethod
    def choices(cls):
        return (
            (cls.Boolean, 'Boolean (Yes/No)'),
            (cls.FreeText, 'Free Text'),
            (cls.Scale, 'Scale'),
        )


class SelfAssessmentAlert(models.Model):
    """
    A simple model to allow a SelfAssessment Questionnaire to be created.

    created_at  the date/time the questionnaire was created

    publish_date    the date/time the questionnaire was published. This is set automatically in save()

    content_type    Self Reflection Alerts are returned to users in alerts. This property tells the front end to link it
                            to the corresponding self-assessment view
    """
    created_at = models.DateTimeField(auto_created=True, null=False, blank=False, default=datetime.now())
    publish_date = models.DateTimeField(null=False, blank=False, default=datetime.now(),
                                        help_text='This is the date and time at which users will recieve the Self Reflection alert from.')

    @property
    def content_type(self):
        return ContentType.SELF_ASSESSMENT

    def __str__(self):
        return 'Platform-wide Self Assessment Alert sent out at ' + str(self.publish_date)


class Questionnaire(models.Model):
    """
        The Questionnaire model. Questionnaires can be created by employers for their company's employees. They can
        also be created in the admin panel, but they must be assigned to a company, or be of a WellbeingQuestionnaire
        type to reach an end user. A Questionnaire can also be set as the company's introductory_questionnaire. This
        uses the same model but changes frontend behaviour (the employer edits the questionnaire in settings rather than
        the usual feed, and the employee responds to it immediately after their registration)

        Attributes:
            title   the title of the questionnaire
            description the description of the questionnaire
            company the company the Questionnaire is associated with. Only users of this company will receive it
            created_at  the date/time the questionnaire was created
            is_published    boolean indicating if the questionnaire has been shared with employees
            publish_date    the date/time the questionnaire was published. This is set automatically in save()
            expiry_date the deadline for users to respond to the questionnaire. After this date they will not be able
                        to respond anymore
            content_type    questionnaires are returned to users in alerts. This property tells the front end to link it
                            to the corresponding questionnaire view
            questions   returns the list of Questions associated with this Questionnaire
            percentage_responded    property for accessing the percentage of employees which have submitted responses
            is_introductory_questionnaire   detects if this questionnaire has been assigned as an introductory
                                            questionnaire for a company
    """
    title = models.CharField(max_length=255, null=False, blank=True, default='')
    description = models.TextField(default='', null=False, blank=True)
    company = models.ForeignKey(to=Company, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_created=True, null=False, blank=False)
    is_published = models.BooleanField(null=False, blank=False, default=False,
                                       help_text='set to true to publish the questionnaire and notify users')
    publish_date = models.DateTimeField(null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=False, help_text='the date past which users can no longer respond')
    # copy of is_published for checking if it has changed
    __original_is_published = None

    @property
    def content_type(self):
        return ContentType.QUESTIONNAIRE

    @property
    def questions(self):
        qqs = QuestionnaireQuestion.objects.filter(questionnaire=self).order_by('order')

        # return the questions in order
        questions = []
        for q in qqs:
            questions.append(q.question)

        return questions

    # property for accessing the percentage of employees which have submitted responses
    @property
    def percentage_responded(self):
        # get count of employees in the company
        employee_count = OptimeUser.objects.filter(company=self.company).count()

        # avoid division by 0
        if employee_count == 0:
            return 0

        # get count of responses to questionnaire
        response_count = QuestionnaireResponse.objects.filter(questionnaire=self).count()

        # return percentage of employees with responses
        return (response_count / employee_count) * 100

    @property
    def is_introductory_questionnaire(self):
        return Company.objects.filter(introductory_questionnaire=self).exists()

    def add_question(self, question, order=1):
        q = QuestionnaireQuestion(questionnaire=self, question=question, order=order)
        q.save()

    def clear_questions(self):
        qs = QuestionnaireQuestion.objects.filter(questionnaire=self)
        for q in qs:
            q.delete()

    def set_questions(self, questions):
        self.clear_questions()
        for question in questions:
            self.add_question(question)

    def save(self, *args, **kwargs):
        is_create = False

        # detect creation
        if self.pk is None:
            is_create = True

        # if is_published is true for the first time, trigger email notification for company members
        if self.is_published is True and (is_create or self.is_published != self.__original_is_published):
            self.publish_date = datetime.now()

            if self.company is not None:
                # get all company members
                recipients = OptimeUser.objects.filter(company_id=self.company_id)

                for recipient in recipients:
                    send_notification_task.apply_async((recipient.pk, self.pk), countdown=60)

        self.__original_is_published = self.is_published

        super(Questionnaire, self).save(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        super(Questionnaire, self).__init__(*args, **kwargs)
        self.__original_is_published = self.is_published

    def __str__(self):
        company = "None"

        if self.company is not None:
            company = self.company.name

        return self.title + "( " + company + ")"


# Questionnaire implementation for the wellbeing Questionnaire. This is a template and there can only be one
class WellbeingQuestionnaire(Questionnaire):
    """
        An extension of the Questionnaire model. This questionnaire can only be edited via the admin panel, and defines
        the template for the special wellbeing questionnaires which are periodically sent to users of all companies. For
        this reason there can only be one instance of this model (asserted in save()). The length of time before the
        wellbeing questionnaire is sent to users again is defined in the app settings
    """

    def save(self, *args, **kwargs):
        # detect creation
        is_creation = False

        if not self.pk:
            # only one object of this model can exist at one time
            if WellbeingQuestionnaire.objects.exists():
                # if you'll not check for self.pk
                # then error will also raised in update of exists model
                raise ValidationError('There can be only one Wellbeing Questionnaire! Please edit the existing one')

            is_creation = True

        super(WellbeingQuestionnaire, self).save(*args, **kwargs)

        if is_creation is True:
            # woo hoo this is the creation of the only instance of this model
            # set up our first cycle of questionnaires. This task will perpetuate itself at regular intervals
            # NOTE: countdown ensures that the questionnaire exists in the backend
            setup_wellbeing_questionnaire_instance_task.apply_async((self.pk,), countdown=10)


class WellbeingQuestionnaireInstance(Questionnaire):
    """
        Instance of the WellbeingQuestionnaire. As the WellbeingQuestionnaire is sent periodically, there needs to be
        multiple instances of it created with a new created_at for each one. An instance is also created per company,
        so that other than its creation the instance can behave in the same way as any other Questionnaire
    """
    pass


class Question(models.Model):
    """
        The Question model. One question can be used by many Questionnaires (and many questions can exist in many
        Questionnaires). Questions which have been created in the admin panel can be assigned to any Questionnaire,
        but employers can create custom questions as well, and these will only be visible to their company

        Attributes:
            title   the title of the question. 'How was your day?'
            response_type   the type of response required by the question. For example it may be a sliding scale
            is_user_created boolean indicating if it was created by a user (employer) or via the admin panel. Admin
                            Questions are visible to all companies, user Questions are only visible to their company
            min_label   only used by sliding scale response_type questions. The label attached to 1
            max_label   only used by sliding scale response_type questions. The label attached to 10
            max_average_label   only used by sliding scale response_type questions. The label attached to 7
            min_average_label   only used by sliding scale response_type questions. The label attached to 2
            ok_average_label   only used by sliding scale response_type questions. The label attached to 5
            pillar  category of wellbeing associated to the Question. If this is a wellbeing Questionnaire this will
                    affect the reports on employee wellbeing, which are organised into these categories
            self_assessment  allows users to decide if they want this to be self assessed
    """
    title = models.CharField(max_length=120, null=False, blank=False, help_text='Question text')
    response_type = models.CharField(max_length=16, choices=QuestionTypeStatus.choices(),
                                     default=QuestionTypeStatus.Boolean)
    is_user_created = models.BooleanField(null=False, blank=False, default=False)
    min_label = models.CharField(max_length=32, null=True, blank=True,
                                 help_text='Label for the minimum option on the scale. Only used in scale question types')
    max_label = models.CharField(max_length=32, null=True, blank=True,
                                 help_text='Label for the maximum option on the scale. Only used in scale question types')
    min_average_label = models.CharField(max_length=32, null=True, blank=True,
                                 help_text='Label for the average minimum option on the scale. Only used in scale question types')
    max_average_label = models.CharField(max_length=32, null=True, blank=True,
                                 help_text='Label for the average maximum option on the scale. Only used in scale question types')
    ok_average_label = models.CharField(max_length=32, null=True, blank=True,
                                 help_text='Label for the average option on the scale. Only used in scale question types')
    pillar = models.CharField(max_length=16, choices=PillarTypeStatus.choices(), default=PillarTypeStatus.Mental)

    self_assessment = models.BooleanField(null=False, blank=False, default=False)

    show_in_self_reflection_modal = models.BooleanField(default=False,
                                                        help_text='Should this question be shown in the anytime Self-Reflection modal, accessible from the sidebar? (Please mark this question as Self Assessment too so the answers show on the radar chart!).')

    def __str__(self):
        return self.title


class QuestionnaireQuestion(models.Model):
    """
        The many to many relationship between Questionnaire and Question

        Attributes:
            questionnaire   the questionnaire in the relationship
            question        the question in the relationship
            order   the order in the questionnaire. The lower the number the faster the user responds to it
            company shortcut to the company property of the parent questionnaire
    """
    questionnaire = models.ForeignKey(to=Questionnaire, on_delete=models.CASCADE, blank=False, null=False,
                                      related_name='questionnaire_questions')
    question = models.ForeignKey(to=Question, on_delete=models.CASCADE, blank=False, null=False,
                                 related_name='instances')
    order = models.PositiveIntegerField(null=False, blank=False)

    @property
    def company(self):
        return self.questionnaire.company

    def delete(self, *args, **kwargs):
        # delete related question if it is a custom question
        if self.question.is_user_created:
            self.question.delete()

        super(QuestionnaireQuestion, self).delete(*args, **kwargs)

    def __str__(self):
        return "question " + str(self.order) + " of questionnaire " + str(self.questionnaire)


class QuestionnaireResponse(models.Model):
    """
        The model for a user's response to a Questionnaire

        Attributes:
            user    the user who submitted this response
            questionnaire   the questionnaire they were responding to
            created_at  timestamp of the response
    """
    user = models.ForeignKey(to=OptimeUser, on_delete=models.CASCADE, blank=False, null=False)
    questionnaire = models.ForeignKey(to=Questionnaire, on_delete=models.CASCADE, blank=False, null=False,
                                      related_name='responses')
    created_at = models.DateTimeField(auto_created=True, null=False, blank=False)

    def __str__(self):
        return "user " + str(self.user_id) + " response to questionnaire " + str(self.questionnaire_id)


class QuestionnaireQuestionResponse(models.Model):
    """
        One QuestionnaireResponse has many QuestionnaireQuestionResponses. This models the individual responses to
        Questions. It is an abstract base class, as the response data will depend on the response_type attribute of
        the Question object
    """
    questionnaire_response = models.ForeignKey(to=QuestionnaireResponse, on_delete=models.CASCADE, blank=False,
                                               null=False, related_name='%(app_label)s_%(class)s_response_questions')
    questionnaire_question = models.ForeignKey(to=QuestionnaireQuestion, on_delete=models.CASCADE, blank=False,
                                               null=False, related_name='%(app_label)s_%(class)s_responses')

    class Meta:
        abstract = True


class BooleanQuestionnaireQuestionResponse(QuestionnaireQuestionResponse):
    """
        Implementation of QuestionnaireQuestionResponses with a boolean response type
    """
    response = models.BooleanField(null=False, blank=False, default=False)


class FreeTextQuestionnaireQuestionResponse(QuestionnaireQuestionResponse):
    """
        Implementation of QuestionnaireQuestionResponses with a text field response type
    """
    response = models.TextField(default='', null=False, blank=True)


class ScaleQuestionnaireQuestionResponse(QuestionnaireQuestionResponse):
    """
        Implementation of QuestionnaireQuestionResponses with a scale response type (a number between 1 and 5)
    """
    response = models.PositiveIntegerField(default=3, null=False, blank=False, validators=[MinValueValidator(1),
                                                                                           MaxValueValidator(5)])


class SelfAssessmentQuestionResponse(models.Model):
    """
        Implementation of self assessment question response linking to a question and the response as a integer
    """
    question = models.ForeignKey(to=Question, on_delete=models.CASCADE, blank=False,
                                 null=False)

    response = models.IntegerField()

    user = models.ForeignKey(to=OptimeUser, on_delete=models.CASCADE, blank=False,
                             null=True)


# celery task for submitting email notification on new questionnaire
@app.task(name="send_notification_task")
def send_notification_task(user_id, questionnaire_id):
    """
        Celery task for asynchronously sending a notification to a user, informing them that there is a new
        questionnaire available for them to complete

        :param user_id: the user to notify
        :param questionnaire_id:    the questionnaire to notify them about
    """
    # pull in user and questionnaire
    try:
        questionnaire = Questionnaire.objects.get(pk=questionnaire_id)
        user = OptimeUser.objects.get(pk=user_id)

        email_context = {'questionnaire_title': questionnaire.title}

        subject = 'A New Questionnaire is Available'
        html_content = render_to_string('email/new_questionnaire.html', email_context)

        # send email notification if required
        if user.email_opt_in is True and user.email is not None and len(user.email) > 4:
            send_mail(
                subject=subject,
                message='',
                from_mail=FROM_EMAIL,
                recipient_list=[user.email],
                fail_silently=False,
                html_message=html_content,
            )

    except Questionnaire.DoesNotExist:
        logger.warn('notification not sent, questionnaire did not exist')

    except OptimeUser.DoesNotExist:
        logger.warn('notification not sent, user did not exist')


@app.task(name="setup_wellbeing_questionnaire_instance_task")
def setup_wellbeing_questionnaire_instance_task(wellbeing_questionnaire_id):
    """
        Celery task for asynchronously preparing a wellbeing questionnaire cycle. This entails creating new
        WellbeingQuestionnaireInstance objects, one per company, with the current date, and publishing them. This will
        trigger the users of these companies being notified of the questionnaires existence so that they can respond.
        This task will then schedule itself to complete again after waiting the period of the wellbeing questionnaire
        cycle, defined in settings

        :param wellbeing_questionnaire_id:    the id for the template of WellbeingQuestionnaire type
    """

    # pull in questionnaire
    try:
        q = WellbeingQuestionnaire.objects.get(pk=wellbeing_questionnaire_id)
        now = datetime.now()
        next_cycle = now + timedelta(seconds=SECONDS_WELLBEING_QUESTIONNAIRE_CYCLE)

        # create one wellbeing questionnaire instance per company
        for company in Company.objects.all():
            instance = \
                WellbeingQuestionnaireInstance(title=q.title, description=q.description, created_at=now,
                                               company=company,
                                               is_published=True, expiry_date=next_cycle)

            # NOTE: this will schedule emails for all users
            instance.save()

            # copy the questions across
            for questionnaire_question in q.questionnaire_questions.all():
                instance.add_question(questionnaire_question.question, questionnaire_question.order)

        # schedule self for next month
        setup_wellbeing_questionnaire_instance_task.apply_async((wellbeing_questionnaire_id,),
                                                                countdown=SECONDS_WELLBEING_QUESTIONNAIRE_CYCLE)

    except WellbeingQuestionnaire.DoesNotExist:
        logger.warn('unable to set up wellbeing instance, questionnaire did not exist!')
