# Generated by Django 2.2.6 on 2019-10-24 09:42

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('questionnaires', '0013_auto_20191014_1405'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='pillar',
            field=models.CharField(
                choices=[('mental', 'Mental and Emotional'), ('physical', 'Physical'), ('professional', 'Professional'),
                         ('environmental', 'Environmental'), ('social', 'Social'), ('financial', 'Financial')],
                default='mental', max_length=16),
        ),
    ]
