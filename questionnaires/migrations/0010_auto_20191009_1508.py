# Generated by Django 2.2.6 on 2019-10-09 15:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('questionnaires', '0009_auto_20191009_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questionnaire',
            name='expiry_date',
            field=models.DateField(help_text='the date past which users can no longer respond'),
        ),
    ]
