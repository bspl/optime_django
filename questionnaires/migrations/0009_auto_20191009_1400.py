# Generated by Django 2.2.6 on 2019-10-09 14:00

import django.core.validators
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('questionnaires', '0008_auto_20191009_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='max_label',
            field=models.CharField(blank=True,
                                   help_text='Label for the maximum option on the scale. Only used in scale question types',
                                   max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='min_label',
            field=models.CharField(blank=True,
                                   help_text='Label for the minimum option on the scale. Only used in scale question types',
                                   max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='response_type',
            field=models.CharField(
                choices=[('boolean', 'Boolean (Yes/No)'), ('free_text', 'Free Text'), ('scale', 'Scale')],
                default='boolean', max_length=16),
        ),
        migrations.CreateModel(
            name='ScaleQuestionnaireQuestionResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('response', models.PositiveIntegerField(default=5,
                                                         validators=[django.core.validators.MinValueValidator(1),
                                                                     django.core.validators.MaxValueValidator(10)])),
                ('questionnaire_question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                                             related_name='questionnaires_scalequestionnairequestionresponse_responses',
                                                             to='questionnaires.QuestionnaireQuestion')),
                ('questionnaire_response', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                                             related_name='questionnaires_scalequestionnairequestionresponse_response_questions',
                                                             to='questionnaires.QuestionnaireResponse')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
