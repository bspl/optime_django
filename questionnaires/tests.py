from datetime import datetime, timedelta

from oauth2_provider.admin import Application
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient

from companies.models import Company
from optimeusers.models import OptimeUser, UserTypeStatus
from .models import Questionnaire, Question


class QuestionnairesTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id,
                                                    client_secret=self.oauth_client_secret)
        self.company = Company()
        self.company.save()

    def setUpLoggedInEmployer(self):
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test',
                               user_type=UserTypeStatus.Employer, company=self.company)
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpEmployee(self):
        self.user = OptimeUser(email='test1@mactest.co.uk', first_name='Test1', last_name='Mactest1', username='test1',
                               user_type=UserTypeStatus.Employee, company=self.company)
        self.user.save()

    def setUpQuestionnaire(self):
        three_days_time = datetime.today() + timedelta(days=3)
        self.questionnaire = Questionnaire(company=self.user.company, created_at='2019-11-07', is_published=False,
                                           expiry_date=three_days_time)
        self.questionnaire.save()

    # NOTE: requires redis-server to be running
    def test_publish(self):
        self.setUpLoggedInEmployer()
        self.setUpQuestionnaire()

        url = reverse('api-questionnaire-publish')
        response = self.client.post(url, data={'id': self.questionnaire.id})

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        questionnaire = Questionnaire.objects.get(id=self.questionnaire.pk)

        self.assertEqual(questionnaire.is_published, True)

    def test_publish_no_permission(self):
        self.setUpLoggedInEmployer()
        three_days_time = datetime.today() + timedelta(days=3)
        questionnaire = Questionnaire(company=None, created_at=datetime.now(), is_published=False,
                                      expiry_date=three_days_time)
        questionnaire.save()

        url = reverse('api-questionnaire-publish')
        response = self.client.post(url, data={'id': questionnaire.id})

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        self.setUpLoggedInEmployer()
        self.setUpQuestionnaire()

        url = reverse('api-questionnaire-detail', args=[self.questionnaire.id])

        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        try:
            Questionnaire.objects.get(id=self.questionnaire.pk)
            # object should have been deleted
            self.assertTrue(False)

        except Questionnaire.DoesNotExist:
            # object deleted, pass the test
            self.assertTrue(True)

    def test_delete_no_permission(self):
        self.setUpLoggedInEmployer()
        three_days_time = datetime.today() + timedelta(days=3)
        questionnaire = Questionnaire(company=None, created_at=datetime.now(), is_published=False,
                                      expiry_date=three_days_time)
        questionnaire.save()

        url = reverse('api-questionnaire-detail', args=[questionnaire.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_questionnaire_questions(self):
        self.setUpLoggedInEmployer()
        self.setUpQuestionnaire()

        # add some questions
        question_a = Question(title='Test')
        question_b = Question(title='Test')
        question_a.save()
        question_b.save()
        self.questionnaire.set_questions([question_a, question_b])

        url = reverse('api-questionnaire-questions', args=[self.questionnaire.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        returned_questions = response.data.get('questions')
        self.assertEqual(len(returned_questions), len(self.questionnaire.questions.all()))




