from django.conf.urls import url

from .views import PublishQuestionnaireView, QuestionnaireDetailView, QuestionnaireQuestionView, \
    QuestionnaireQuestionDetailView

urlpatterns = [
    url(r'^questionnaire/question/(?P<pk>[0-9]+)/$', QuestionnaireQuestionDetailView.as_view(),
        name='api-questionnaire-question-detail'),
    url(r'^publish/$', PublishQuestionnaireView.as_view(), name='api-questionnaire-publish'),
    url(r'^(?P<pk>[0-9]+)/questions/$', QuestionnaireQuestionView.as_view(),
        name='api-questionnaire-questions'),
    url(r'^(?P<pk>[0-9]+)/$', QuestionnaireDetailView.as_view(), name='api-questionnaire-detail'),
]
