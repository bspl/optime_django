import modelclone
from django.contrib import admin

from .models import QuestionnaireQuestion, Question, Questionnaire, QuestionnaireResponse, \
    BooleanQuestionnaireQuestionResponse, FreeTextQuestionnaireQuestionResponse, WellbeingQuestionnaire, \
    ScaleQuestionnaireQuestionResponse, SelfAssessmentAlert


# inline display for questionnaireQuestions property
class QuestionnaireQuestionInline(admin.TabularInline):
    model = QuestionnaireQuestion

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "question":
            kwargs["queryset"] = Question.objects.filter(is_user_created=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# inline display for QuestionnaireQuestionResponse property
class BooleanQuestionnaireQuestionResponseInline(admin.TabularInline):
    model = BooleanQuestionnaireQuestionResponse


class FreeTextQuestionnaireQuestionResponseInline(admin.TabularInline):
    model = FreeTextQuestionnaireQuestionResponse


class ScaleQuestionnaireQuestionResponseInline(admin.TabularInline):
    model = ScaleQuestionnaireQuestionResponse


class QuestionnaireAdmin(modelclone.ClonableModelAdmin):
    fields = ('title', 'company', 'created_at', 'expiry_date', 'is_published')
    inlines = [QuestionnaireQuestionInline]

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # exclude WellbeingQuestionnaire model
        # NOTE: there will only ever be one WellbeingQuestionnaire
        wq = WellbeingQuestionnaire.objects.all()
        print(str(wq))

        if wq.exists():
            return qs.exclude(pk=wq[0].pk)

        return qs


class WellbeingQuestionnaireAdmin(admin.ModelAdmin):
    exclude = ('expiry_date', 'company', 'is_published', 'publish_date')
    inlines = [QuestionnaireQuestionInline]


class QuestionnaireResponseAdmin(admin.ModelAdmin):
    fields = ('user', 'questionnaire', 'created_at')
    inlines = [BooleanQuestionnaireQuestionResponseInline,
               FreeTextQuestionnaireQuestionResponseInline,
               ScaleQuestionnaireQuestionResponseInline]


class QuestionAdmin(admin.ModelAdmin):
    fields = (
    'title', 'response_type', 'min_label', 'min_average_label',
     'max_average_label', 'ok_average_label',
     'max_label', 'pillar', 'self_assessment', 'show_in_self_reflection_modal')

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # only return questions not created by the user
        return qs.filter(is_user_created=False)


class UserCreatedQuestion(Question):
    class Meta:
        proxy = True


class UserCreatedQuestionAdmin(QuestionAdmin):
    fields = QuestionAdmin.fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # only return questions created by the user
        return qs.filter(is_user_created=True)


admin.site.register(Questionnaire, QuestionnaireAdmin)
admin.site.register(WellbeingQuestionnaire, WellbeingQuestionnaireAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(UserCreatedQuestion, UserCreatedQuestionAdmin)
admin.site.register(QuestionnaireResponse, QuestionnaireResponseAdmin)
admin.site.register(SelfAssessmentAlert)
