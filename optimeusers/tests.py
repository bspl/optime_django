from datetime import datetime

from oauth2_provider.admin import Application
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient

from companies.models import Company
from content.models import ArticleContent
from content.views import get_new_alerts
from .models import OptimeUser, UserTypeStatus


class OptimeUserTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.oauth_client_id = 'TEST'
        self.oauth_client_secret = 'TEST'
        self.oauth_app = Application.objects.create(client_id=self.oauth_client_id,
                                                    client_secret=self.oauth_client_secret)

    def setUpLoggedInEmployer(self):
        company = Company()
        company.save()
        self.user = OptimeUser(email='test@mactest.co.uk', first_name='Test', last_name='Mactest', username='test',
                               user_type=UserTypeStatus.Employer, company=company)
        self.user.save()
        self.client.force_authenticate(user=self.user)

    def setUpEmployees(self):
        self.employee = OptimeUser(email='empl@mactest.co.uk', first_name='Test', last_name='Mactest', username='test2',
                                   user_type=UserTypeStatus.Employee, company=self.user.company)
        self.employee.save()

    def test_delete_employee(self):
        self.setUpLoggedInEmployer()
        self.setUpEmployees()

        self.assertEqual(self.employee.is_active, True)

        url = reverse('api-employee-detail', args=[self.employee.pk])

        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        employee = OptimeUser.objects.get(pk=self.employee.pk)
        self.assertEqual(employee.is_active, False)

    def test_clear_alerts(self):
        self.setUpLoggedInEmployer()

        last_last_login = self.user.last_last_login

        # test that alert gets generated for some new content
        article = ArticleContent(title='Test', created_at=datetime.now(), publish_date=datetime.now())
        article.save()
        alerts = get_new_alerts(self.user)
        self.assertEqual(len(alerts), 1)

        url = reverse('api-clear-alerts')

        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(last_last_login, self.user.last_last_login)

        # alerts should now be cleared
        alerts = get_new_alerts(self.user)

        self.assertEqual(len(alerts), 0)
