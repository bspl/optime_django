from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from awards.models import Awards
from companies.models import Company, CompanyAwards


class UserTypeStatus(object):
    """Defines user types used in the application"""
    Employee = 'employee'
    Employer = 'employer'
    Admin = 'admin'

    @classmethod
    def choices(cls):
        return (
            (cls.Employer, 'Employer'),
            (cls.Admin, 'Admin'),
            (cls.Employee, 'Employee'),
        )


class GenderStatus(object):
    """Defines valid gender options"""
    Male = 'male'
    Female = 'female'
    Other = 'other'

    @classmethod
    def choices(cls):
        return (
            (cls.Male, 'Male'),
            (cls.Female, 'Female'),
            (cls.Other, 'Other'),
        )


class AgeRangeStatus(object):
    """Defines valid age options"""
    SeventiesPlus = 'seventies_plus'
    Sixties = 'sixties'
    Fifties = 'fifties'
    Forties = 'forties'
    Thirties = 'thirties'
    Twenties = 'twenties'
    UnderTwentyOne = 'under_twenty_one'

    @classmethod
    def choices(cls):
        return (
            (cls.SeventiesPlus, '70+'),
            (cls.Sixties, '61-70'),
            (cls.Fifties, '51-60'),
            (cls.Forties, '41-50'),
            (cls.Thirties, '31-40'),
            (cls.Twenties, '21-30'),
            (cls.UnderTwentyOne, 'under 21'),
        )


class Ethnicity(object):
    """Defines valid ethnicity options"""
    British = 'british'
    Irish = 'irish'
    Gypsy = 'gypsy'
    WhiteOther = 'white_other'
    WhiteBlackCaribbean = 'white_black_caribbean'
    WhiteBlackAfrican = 'white_black_african'
    WhiteAsian = 'white_asian'
    Mixed_Other = 'mixed_other'
    Indian = 'indian'
    Pakistani = 'pakistani'
    Bangladeshi = 'bangladeshi'
    Chinese = 'chinese'
    AsianOther = 'asian_other'
    African = 'african'
    Caribbean = 'caribbean'
    BlackOther = 'black_other'
    Arab = 'arab'
    Other = 'other'
    PreferNotToSay = 'prefer_not_to_say'

    @classmethod
    def choices(cls):
        return (
            (cls.British, 'british'),
            (cls.Irish, 'irish'),
            (cls.Gypsy, 'gypsy'),
            (cls.WhiteOther, 'white other'),
            (cls.WhiteBlackCaribbean, 'white black caribbean'),
            (cls.WhiteBlackAfrican, 'white black african'),
            (cls.WhiteAsian, 'white asian'),
            (cls.Mixed_Other, 'mixed_other'),
            (cls.Indian, 'indian'),
            (cls.Pakistani, 'pakistani'),
            (cls.Bangladeshi, 'bangladeshi'),
            (cls.Chinese, 'chinese'),
            (cls.AsianOther, 'asian other'),
            (cls.African, 'african'),
            (cls.Caribbean, 'caribbean'),
            (cls.BlackOther, 'black other'),
            (cls.Arab, 'arab'),
            (cls.Other, 'other'),
            (cls.PreferNotToSay, 'prefer not to say'),
        )


class OccupationalActivityStatus(object):
    """Defines options for the occupational activity field, describing how strenuous the user's work is"""
    MainlyDeskBased = 'mainly_desk'
    MainlyStanding = 'mainly_standing'
    MainlyDriving = 'mainly_driving'
    DeskBasedDriving = 'desk_driving'
    DeskBasedStanding = 'desk_standing'
    StandingAndDriving = 'standing_driving'

    @classmethod
    def choices(cls):
        return (
            (cls.MainlyDeskBased, 'Mainly desk based'),
            (cls.MainlyStanding, 'Mainly standing'),
            (cls.MainlyDriving, 'Mainly driving'),
            (cls.DeskBasedDriving, 'Desk based and driving'),
            (cls.DeskBasedStanding, 'Desk based and standing'),
            (cls.StandingAndDriving, 'Standing and driving'),
        )


class OptimeUser(AbstractUser):
    """
        The User model. Used in authentication and all things tracking the user of the app

        Attributes:
            company the company which the user belongs to. This can be None
            user_type   the type of user, for example 'Employee'. Changes app behaviour and user permissions
            age_range   a category of age group which the user belongs to
            gender  the user's gender selection
            occupational_activity   a category describing how strenuous the user's work is
            height  height in cm
            weight  weight in kg
            dairy_free  boolean indicating if the user's dietary requirements include an absence of dairy
            meat_free   boolean indicating if the user's dietary requirements include an absence of meat
            gluten_free boolean indicating if the user's dietary requirements include an absence of gluten
            vegan boolean indicating if the user is a vegan
            email_opt_in    boolean indicating if the user has opted in to receive email communications
            email_confirmed set to True when a user accepts an invitation to join the app and activates their account
            last_last_login the previous value of last_login. This is used to determine which content to get alerts with
            active_goal returns the current active goal for this user, if one exists. This is a goal which they have
                        selected and are working toward achieving (see Goal and UserGoal models)
            ethnicity describes users ethnicity
    """
    company = models.ForeignKey(to=Company, on_delete=models.CASCADE, blank=True, null=True)
    user_type = models.CharField(max_length=16, choices=UserTypeStatus.choices(), default=UserTypeStatus.Employee)
    age_range = models.CharField(max_length=16, choices=AgeRangeStatus.choices(), default=AgeRangeStatus.Twenties)
    gender = models.CharField(max_length=16, choices=GenderStatus.choices(), default=GenderStatus.Other)
    occupational_activity = models.CharField(max_length=16, choices=OccupationalActivityStatus.choices(),
                                             default=OccupationalActivityStatus.MainlyDeskBased,
                                             help_text='amount of strenuous activity involved in your occupation')
    height = models.PositiveIntegerField(null=True, blank=True, default=0, help_text='height in cm')
    weight = models.DecimalField(null=True, blank=True, default=0, max_digits=5, decimal_places=2,
                                 help_text='weight in kg')
    dairy_free = models.BooleanField(null=False, blank=False, default=False,
                                     help_text='tick if you don\'t consume dairy')
    meat_free = models.BooleanField(null=False, blank=False, default=False, help_text='tick if you don\'t eat meat')
    gluten_free = models.BooleanField(null=False, blank=False, default=False, help_text='are you gluten free?')
    vegan = models.BooleanField(null=False, blank=False, default=False, help_text='are you a vegan?')
    email_opt_in = models.BooleanField(null=False, blank=False, default=False,
                                       help_text='opt in to email communication')
    email_confirmed = models.BooleanField(null=False, blank=False, default=False)
    # the previous value of last_login. This is used to determine which content to get alerts with
    last_last_login = models.DateTimeField(null=True)

    ethnicity = models.CharField(max_length=17, choices=Ethnicity.choices(), default=Ethnicity.PreferNotToSay,
                                 help_text='select your ethnicity')
    workshop_goal = models.ForeignKey(to='goals.VideoGoal', on_delete=models.SET_NULL, blank=True, null=True)


    @property
    def active_goal(self):
        goal = self.goals.filter(is_active=True)

        if not goal.exists():
            return None

        return goal[0]

    def __str__(self):
        company = 'none'
        if self.company is not None:
            company = self.company.name

        return self.email + ' - Employer: ' + company

class UserCompanyAward(models.Model):
    """
    Link users to awards
    """
    user = models.ForeignKey(to=OptimeUser, on_delete=models.CASCADE, blank=False, null=False)
    company_award = models.ForeignKey(to=CompanyAwards, on_delete=models.CASCADE, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user.email) + ' has been awarded ' + str(self.company_award.award)

    class Meta:
        verbose_name_plural = "User Company Awards"