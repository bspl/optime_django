from celery.utils.log import get_task_logger
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.template import loader

from optime.celery import app
from optime.settings import FROM_EMAIL
from optimeusers.models import OptimeUser

logger = get_task_logger(__name__)


# celery task for submitting email notification on new questionnaire
@app.task(name="send_invite_task")
def send_invite_task(user_id, invite_link):
    """
        Celery task for asynchronously sending an invite to join Optime. This is triggered by an Employer inviting a
        new Employee to their company and entering their email address. A user account is provisioned with basic details
        but marked as unconfirmed

        :param user_id: the ID of the user which is to join (an account with basic details has been created)
        :param invite_link: the invite URL to include in the email
    """
    try:
        user = OptimeUser.objects.get(pk=user_id)
        print(">?>?>?>?>?>?>?>?>?>?>?> email send????", user_id, invite_link, user)
        email_context = {'invite_link': invite_link, 'company': user.company.name}

        subject = 'Invitation to OptiMe!'
        html_content = loader.render_to_string('email/employee_invite.html', email_context)

        # send email notification if required
        if user.email is not None and len(user.email) > 4:
            send_mail(
                subject=subject,
                message='',
                from_email=FROM_EMAIL,
                recipient_list=[user.email],
                fail_silently=False,
                html_message=html_content,

            )

    except OptimeUser.DoesNotExist:
        logger.error('abandoning email as user did not exist!')
