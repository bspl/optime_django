from django.conf.urls import url

from .views import EmployeeDetailView, ForgotPasswordView, ClearAlertsView

urlpatterns = [
    url(r'^alerts/clear/$', ClearAlertsView.as_view(), name='api-clear-alerts'),
    url(r'^forgot/(?P<pk>[0-9]+)/$', ForgotPasswordView.as_view(), name='api-forgot-password'),
    url(r'^(?P<pk>[0-9]+)/$', EmployeeDetailView.as_view(), name='api-employee-detail'),
]
