from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    """
        class used in generating a token used to authenticate a one-time login link (registration and forgot password)
    """

    def _make_hash_value(self, user, timestamp):
        return (
                six.text_type(user.pk) + six.text_type(timestamp) +
                six.text_type(user.email_confirmed)
        )


account_activation_token = AccountActivationTokenGenerator()
