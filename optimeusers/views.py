from datetime import datetime

from django.urls import reverse
from django.utils.http import urlsafe_base64_encode
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from optime.settings import BASE_URL, MAIL_URL
from optimeusers.tokens import account_activation_token
from questionnaires.permissions import EmployerPermission
from .models import OptimeUser
from .tasks import send_invite_task


def get_password_link(url, user):
    """
        Auxiliary function generates a one-time password link
        :param url: the URL the one-time link should direct the user to
        :param user: the user to authenticate in the token
        :return: a one-time login link
    """
    user_id_string = str(user.pk).encode("utf-8")
    return MAIL_URL + reverse(url, args=[urlsafe_base64_encode(user_id_string),
                                         account_activation_token.make_token(user=user)])


class EmployeeDetailView(APIView):
    """
        delete:
        deactivates a user's account. This makes it impossible for this user to login until it is reactivated, and
        revokes their permissions, without deleting their account and all the associated data
    """
    permission_classes = (EmployerPermission,)

    def get_object(self, request, pk):
        try:
            user = OptimeUser.objects.get(pk=pk)
            self.check_object_permissions(request, user)
            return user
        except OptimeUser.DoesNotExist:
            return Response(data={'error': {'user': ['Could not find a user with this id!']}},
                            status=status.HTTP_404_NOT_FOUND)

    # deactivating an employee's account
    def delete(self, request, pk):
        user = self.get_object(request, pk)

        user.is_active = False
        user.save()

        return Response(status=status.HTTP_200_OK)


class ForgotPasswordView(APIView):
    """
        post:
        submits a 'forgot password' request and sends a one-time login link to parameterised user's email. Only an
        employer can request a password reset, and only for an employee of their company
    """
    permission_classes = (EmployerPermission,)

    def get_object(self, request, pk):
        try:
            user = OptimeUser.objects.get(pk=pk)
            self.check_object_permissions(request, user)
            return user
        except OptimeUser.DoesNotExist:
            return Response(data={'error': {'user': ['Could not find a user with this id!']}},
                            status=status.HTTP_404_NOT_FOUND)

    def post(self, request, pk):
        user = self.get_object(request, pk)

        link = get_password_link('frontendsite-forgot-password', user)

        # email this to the employee
        send_invite_task.apply_async((user.pk, link))

        return Response(status=status.HTTP_200_OK)


class ClearAlertsView(APIView):
    """
        post:
        sets the last_login and last_last_login attributes on the user to now. Because of the way the alerts view works
        this clears all their current alerts (see content/views.py)
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        request.user.last_login = datetime.now()
        request.user.last_last_login = request.user.last_login
        request.user.save()

        return Response(status=status.HTTP_200_OK)
