from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import OptimeUser, UserCompanyAward

# class AwardsInline(admin.TabularInline):
#     model = UserAwards

class OptimeUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        ('Personal Details', {'fields': ('age_range', 'gender', 'height', 'weight', 'ethnicity')}),
        ('Employment', {'fields': ('company', 'user_type', 'occupational_activity')}),
        ('Dietary Requirements', {'fields': ('meat_free', 'dairy_free', 'gluten_free', 'vegan')}),
        ('Data', {'fields': ('email_opt_in', 'email_confirmed')}),
        ('Goal', {'fields': ('workshop_goal',)})
    )

class OptimeUserAwardAdmin(admin.ModelAdmin):
    fields = ('user','company_award')


admin.site.register(OptimeUser, OptimeUserAdmin)
admin.site.register(UserCompanyAward, OptimeUserAwardAdmin)
