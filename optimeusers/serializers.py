from django.core.validators import MinValueValidator
from rest_framework import serializers

from companies.serializers import CompanySerializer
from .models import *


class OptimeUserSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=True, allow_blank=False, max_length=255)
    last_name = serializers.CharField(required=True, allow_blank=False, max_length=255)
    company = CompanySerializer(read_only=True)
    user_type = serializers.ChoiceField(read_only=True, choices=UserTypeStatus.choices(), allow_blank=True)
    age_range = serializers.ChoiceField(required=False, choices=AgeRangeStatus.choices(), allow_blank=True)
    gender = serializers.ChoiceField(required=False, choices=GenderStatus.choices(), allow_blank=True)
    occupational_activity = serializers.ChoiceField(required=False, choices=OccupationalActivityStatus.choices(),
                                                    allow_blank=True)
    height = serializers.IntegerField(required=False, validators=[MinValueValidator(0)])
    weight = serializers.DecimalField(required=False, max_digits=5, decimal_places=2, validators=[MinValueValidator(0)])
    dairy_free = serializers.BooleanField(required=True)
    meat_free = serializers.BooleanField(required=True)
    gluten_free = serializers.BooleanField(required=True)
    vegan = serializers.BooleanField(required=True)
    email_opt_in = serializers.BooleanField(required=True)
    email_confirmed = serializers.BooleanField(read_only=True)
    ethnicity = serializers.ChoiceField(choices=Ethnicity.choices(), allow_blank=True, required=False)

    def create(self, validated_data):
        return OptimeUser.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.age_range = validated_data.get('age_range', instance.age_range)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.occupational_activity = validated_data.get('occupational_activity', instance.occupational_activity)
        instance.height = validated_data.get('height', instance.height)
        instance.weight = validated_data.get('weight', instance.weight)
        instance.dairy_free = validated_data.get('dairy_free', instance.dairy_free)
        instance.meat_free = validated_data.get('meat_free', instance.meat_free)
        instance.gluten_free = validated_data.get('gluten_free', instance.gluten_free)
        instance.vegan = validated_data.get('vegan', instance.vegan)
        instance.email_opt_in = validated_data.get('email_opt_in', instance.email_opt_in)
        instance.ethnicity = validated_data.get('ethnicity', instance.ethnicity)

        instance.save()

        return instance
