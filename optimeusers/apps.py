from django.apps import AppConfig


class OptimeusersConfig(AppConfig):
    name = 'optimeusers'
