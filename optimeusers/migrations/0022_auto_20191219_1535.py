# Generated by Django 2.2.6 on 2019-12-19 15:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0016_auto_20191211_1633'),
        ('optimeusers', '0021_auto_20191219_1534'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UserAwards',
            new_name='UserCompanyAwards',
        ),
        migrations.AlterField(
            model_name='usercompanyawards',
            name='award',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='companies.CompanyAwards'),
        ),
    ]
